#ifndef	_ADC_H_
#define	_ADC_H_

#define		BQ24650_EN_PIN		GPIO_Pin_5
#define		BQ24650_EN_TYPE		GPIOA

#define		POWER_ADC_PIN		GPIO_Pin_6
#define		POWER_ADC_TYPE		GPIOA

#define		DC_POWER_ADC_PIN	GPIO_Pin_0
#define		DC_POWER_ADC_TYPE	GPIOB

#define		BAT_FULL_PIN		GPIO_Pin_4
#define		BAT_FULL_TYPE		GPIOA

#define 	ADC_ENABLE_GPIO_PIN			GPIO_Pin_7
#define		ADC_ENABLE_GPIO_TYPE		GPIOA

#define		ADC_PIN_ENABLE()		GPIO_SetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)
#define		ADC_PIN_DISABLE()		GPIO_ResetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)

#define		BQ24650_ENABLE()		GPIO_ResetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)
#define		BQ24650_DISABLE()		GPIO_SetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)

#define		ADC_CHANNEL			ADC_Channel_6

#define		ADC_DCPOWER_CHANNEL	ADC_Channel_8	//PB0��ӦCh8

void Adc_Init(void);

void Open_AdcChannel(void);

void Adc_Reset(void);


#endif



