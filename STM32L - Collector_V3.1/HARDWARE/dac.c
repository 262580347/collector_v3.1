#include "dac.h"
#include "main.h"


//温度量程范围(度)
#define TEMP_RANGE_MAX	60.0
#define TEMP_RANGE_MIN	-20.0

//湿度量程范围(%)
#define HUMI_RANGE_MAX	100.0
#define HUMI_RANGE_MIN	0.0

//大气压力量程范围(hPa)
#define PRESS_RANGE_MAX	1100.0
#define PRESS_RANGE_MIN	300.0

//光照度量程==============================
//#define ILLUMINANCE_RANGE_MAX	188000.0
#define ILLUMINANCE_RANGE_MAX	200000.0
#define ILLUMINANCE_RANGE_MIN	0.0

//输出电压幅值定义========================
#define V_ERR	0.3	//故障时DAC输出电压

#define V_MIN	0.6	//4mA时DAC输出电压
#define V_MAX	3.0	//20mA是DAC输出电压



float g_VddVoltage = 0;    //VDD电源电压全局变量

float GetVddRef( void )
{
	return g_VddVoltage;
}

void Dac1_Init(void)
{
	DAC_InitTypeDef DAC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	/* Enable GPIOA clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/* Configure PA.04 (DAC_OUT1) in analog mode -------------------------*/
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Enable DAC clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	/* DAC channel1 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;

	/* DAC Channel1 Init */
	DAC_Init(DAC_Channel_1, &DAC_InitStructure);
	/* Enable DAC Channel1 */
	DAC_Cmd(DAC_Channel_1, ENABLE);
}

//DAC通道2输出初始化
void Dac2_Init(void)
{
	DAC_InitTypeDef DAC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	/* Enable GPIOA clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/* Configure PA.04 (DAC_OUT1) in analog mode -------------------------*/
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Enable DAC clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	/* DAC channel1 Configuration */
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;

	/* DAC Channel1 Init */
	DAC_Init(DAC_Channel_2, &DAC_InitStructure);
	/* Enable DAC Channel1 */
	DAC_Cmd(DAC_Channel_2, ENABLE);
}
//设置通道1输出电压
//vol:0~3.3V
void Dac1_Set_Vol(u16 OutputVol)
{
	DAC_SetChannel1Data(DAC_Align_12b_R, OutputVol);
}

//设置通道2输出电压
//vol:0~3.3V
void Dac2_Set_Vol(u16 OutputVol)
{
	DAC_SetChannel1Data(DAC_Align_12b_R, OutputVol);
}

//设置DAC输出值
void SetDacValue( u8 ucChannel,u16 nValue )
{
	switch( ucChannel )
	{
		case 1:
			Dac1_Set_Vol( nValue );
			break;
		case 2:
			Dac2_Set_Vol( nValue);
			break;
	}
	
}

//根据数值输出DAC电压，输出与传感器的量程有关。
void SetTempDACValue( u8 ucChannel )
{
	float fTemp = 0.0;		//温度值
	u16 nDACValue = 0;		//DAC值
	float fVoltage = 0.0;	//电压值
	float fCurrent = 0.0;	//电流值
	char str[200] = {0};
	
	fTemp = SHT_GetValue();
	
	//============================================================
	//按照输出0.6--3.0V计算
	//-20度，输出0.6V
	//60 度，输出3.0V
	//输出电压与温度的关系为：V = 0.03 * T + 1.2;
	//输出DAC与温度的关系为：DAC = V / 3.3V * 4095;
	//输出电流与电压的关系为：I = 10*V/1.5 (mA)
	//============================================================
	if( fTemp < TEMP_RANGE_MIN || fTemp > TEMP_RANGE_MAX ) 
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095.0 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue(ucChannel,nDACValue);
		printf("[DAC%d]:温度数据错误=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
	if( GetTHSensorStatus() == TH_SENSOR_STATUS_ERR )
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095.0 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue(ucChannel,nDACValue);
		printf("[DAC%d]:温湿度传感器故障，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,nDACValue,fVoltage,fCurrent);
	}
	else
	{	
		fVoltage = 0.03 * fTemp + 1.2;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue(ucChannel,nDACValue);
		printf("[DAC%d]:温度=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
}

void SetHumiDACValue( u8 ucChannel )
{
	float fTemp = 0.0;		//温度值
	u16 nDACValue = 0;		//DAC值
	float fVoltage = 0.0;	//电压值
	float fCurrent = 0.0;	//电流值
	char str[200] = {'\0'};
	
	fTemp = GetAirHumi();
	
	//============================================================
	//按照输出0.6--3.0V计算
	//0%，输出0.6V
	//100%，输出3.0V
	//输出电压与湿度的关系为：V = 0.024 * H + 0.6;
	//输出DAC与湿度的关系为：DAC = V / 3.3V * 4095;
	//输出电流与电压的关系为：I = 10*V/1.5 (mA)
	//============================================================
	if( fTemp < HUMI_RANGE_MIN || fTemp > HUMI_RANGE_MAX )
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue);
		printf("[DAC%d]:湿度数据错误=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
	else if( GetTHSensorStatus() == TH_SENSOR_STATUS_ERR )
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue);
		printf("[DAC%d]:温湿度传感器故障，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,nDACValue,fVoltage,fCurrent);
		
	}
	else
	{
		fVoltage = 0.024 * fTemp + 0.6;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue);		
		printf("[DAC%d]:湿度=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
}

void SetBMPTempDACValue( u8 ucChannel )
{
	float fTemp = 0.0;		//温度值
	u16 nDACValue = 0;		//DAC值
	float fVoltage = 0.0;	//电压值
	float fCurrent = 0.0;	//电流值
	char str[200] = {'\0'};
	
	fTemp = GetBMPTemp();
	
	//============================================================
	//按照输出0.6--3.0V计算
	//-20度，输出0.6V
	//60 度，输出3.0V
	//输出电压与温度的关系为：V = 0.03 * T + 1.2;
	//输出DAC与温度的关系为：DAC = V / 3.3V * 4095;
	//输出电流与电压的关系为：I = 10*V/1.5 (mA)
	//============================================================
	if( fTemp < TEMP_RANGE_MIN || fTemp > TEMP_RANGE_MAX ) 
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue);
		printf("[DAC%d]:BMP温度数据错误=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
	else if( GetBMP280SensorStatus() == BMP_SENSOR_STATUS_ERR )
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue);
		printf("[DAC%d]:BMP温度传感器故障，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,nDACValue,fVoltage,fCurrent);
		
	}
	else
	{	
		fVoltage = 0.03 * fTemp + 1.2;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue(ucChannel,nDACValue);
		printf("[DAC%d]:BMP温度=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
	
}

void SetBMPPressDACValue( u8 ucChannel )
{
	float fTemp = 0.0;		//压力值
	u16 nDACValue = 0;		//DAC值
	float fVoltage = 0.0;	//电压值
	float fCurrent = 0.0;	//电流值
	char str[200] = {'\0'};
	
	fTemp = GetBMPPress();
	
	//============================================================
	//按照输出0.6--3.0V计算
	// 300hPa，输出0.6V
	//1100hPa，输出3.0V
	//输出电压与压力的关系为：V = 0.003 * P - 0.3;
	//输出DAC与压力的关系为：DAC = V / 3.3V * 4095;
	//输出电流与电压的关系为：I = 10*V/1.5 (mA)
	//============================================================
	if( fTemp < PRESS_RANGE_MIN || fTemp > PRESS_RANGE_MAX ) 
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:压力数据错误=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);

	}
	else if( GetBMP280SensorStatus() == BMP_SENSOR_STATUS_ERR )
	{
		fVoltage = V_ERR;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:BMP压力传感器故障，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,nDACValue,fVoltage,fCurrent);

		
	}
	else
	{	
		fVoltage = (V_MAX - V_MIN) / ( PRESS_RANGE_MAX - PRESS_RANGE_MIN)* fTemp + V_MIN - (V_MAX - V_MIN) / ( PRESS_RANGE_MAX - PRESS_RANGE_MIN)*PRESS_RANGE_MIN;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:压力=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);

	}
	
}

void SetIlluminanceDACValue( u8 ucChannel )
{
	float fTemp = 0.0;		//压力值
	u16 nDACValue = 0;		//DAC值
	float fVoltage = 0.0;	//电压值
	float fCurrent = 0.0;	//电流值
	char str[200] = {'\0'};
	
	fTemp = GetIlluminance();
	
	//============================================================
	//按照输出0.6--3.0V计算
	// 0Lux，输出0.6V
	//188000Lux，输出3.0V
	//输出电压与光照的关系为：V = 0.000012766 * T + 0.6;
	//输出DAC与光照的关系为：DAC = V / 3.3V * 4095;
	//输出电流与电压的关系为：I = 10*V/1.5 (mA)
	//============================================================
	if( (fTemp < ILLUMINANCE_RANGE_MIN) || (fTemp > ILLUMINANCE_RANGE_MAX)  ) 
	{
		fVoltage = 0.3;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:光照度数据错误=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
	}
	else if( GetIlluminanceSensorStatus() == ILLUMINANCE_SENSOR_STATUS_ERR )
	{
		fVoltage = 0.3;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:光照度传感器故障，DAC= %d，电压=%5.3fV，电流=%5.3fmA",ucChannel,nDACValue,fVoltage,fCurrent);		
	}
	else
	{	
		fVoltage = (V_MAX - V_MIN) / ( ILLUMINANCE_RANGE_MAX - ILLUMINANCE_RANGE_MIN)* fTemp + V_MIN - (V_MAX - V_MIN) / ( ILLUMINANCE_RANGE_MAX - ILLUMINANCE_RANGE_MIN)*ILLUMINANCE_RANGE_MIN;
		nDACValue = (UINT16)(fVoltage * 4095 / GetVddRef() );
		fCurrent = 10.0 * fVoltage / 1.5 ;
		SetDacValue( ucChannel, nDACValue );
		printf("[DAC%d]:光照度=%5.3f，DAC= %d，电压=%5.3fV，电流=%5.3fmA\r\n",ucChannel,fTemp,nDACValue,fVoltage,fCurrent);
		//printf("%5.3f  ",fTemp);
	}
	
}


////DAC输出 0.6--3.3 V，通过XTR111输出4-20mA
void DacControl( u16 nMain100ms )
{
	static u16 s_nLast = 0;
	static u8 s_nState = 1;
	
	if(s_nState)
	{
		s_nState = 0;
		g_VddVoltage = Get_VDDValue();		//获取VDD电源电压
//		g_VddVoltage = 3.3;
//		s_nLast = nMain100ms- 60 * ONE_SECOND;  ///////////////需修改
		
		printf("[CPU]基准电压：%2.4fV\r\n", g_VddVoltage);
		
		Dac1_Init();	//DAC1输出初始化
		Dac2_Init();	//DAC2输出初始化
		
		s_nLast = nMain100ms;
		
	}
	
	if( CalculateTime(nMain100ms,s_nLast) < (5 * ONE_SECOND) )
	{
		return;
	}
	
	s_nLast = nMain100ms;
	
	
	//根据编译选项选择输出的数据
	
	//DAC1输出数值选择=========================
	#if ( DAC_CHANNEL_CONF_1 == TH_TEMP )
	//温度输出控制
	SetTempDACValue( DAC_CHANNEL_1 );
	
	#elif ( DAC_CHANNEL_CONF_1 == TH_HUMI )
	
	//湿度输出控制
	SetHumiDACValue( DAC_CHANNEL_1 );
//	DEBUG_OUTPUT("\n\r");
	
	#elif ( DAC_CHANNEL_CONF_1 == BMP_TEMP )
	SetBMPTempDACValue( DAC_CHANNEL_1 );
	
	#elif( DAC_CHANNEL_CONF_1 == BMP_PRESS )
	SetBMPPressDACValue( DAC_CHANNEL_1 )
	
	#elif ( DAC_CHANNEL_CONF_1 == MAX44009_ILLUMINANCE )
	SetIlluminanceDACValue( DAC_CHANNEL_1 )

	#endif
	
	
	//DAC2输出数值选择=========================
	#if ( DAC_CHANNEL_CONF_2 == TH_TEMP )
	//温度输出控制
	SetTempDACValue( DAC_CHANNEL_2 );
	#elif ( DAC_CHANNEL_CONF_2 == TH_HUMI )
	
	//湿度输出控制
	SetHumiDACValue( DAC_CHANNEL_2 );
//	DEBUG_OUTPUT("\n\r");
	
	#elif ( DAC_CHANNEL_CONF_2 == BMP_TEMP )
	SetBMPTempDACValue( DAC_CHANNEL_2 );
	
	#elif( DAC_CHANNEL_CONF_2 == BMP_PRESS )
	SetBMPPressDACValue( DAC_CHANNEL_2 );
	
	#elif ( DAC_CHANNEL_CONF_2 == MAX44009_ILLUMINANCE )
	SetIlluminanceDACValue( DAC_CHANNEL_2 );

	#endif
}


















































































