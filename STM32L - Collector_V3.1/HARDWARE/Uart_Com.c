/**********************************
说明：调试使用的串口代码
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

unsigned char s_ComTestFlag = FALSE;

unsigned char GetComTestFlag(void)
{
	return s_ComTestFlag;
}

void SetComTestFlag(unsigned char isTrue)
{
	s_ComTestFlag = isTrue;
}

void Create_Alive_Message(unsigned int Device_ID)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(308);							//通信协议版本号
	hdr->device_id = swap_dword(Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	send_buf[len++] = 	SENSOR_TYPE_GATEWAY;	//传感器网关  200
	
	send_buf[len++] = 1;	//通道1
	
	send_buf[len++] = 0;
	send_buf[len++] = 100;	//协议版本
	
	send_buf[len++] = XML_BAT_VOLTAGE;	//电压标识
	send_buf[len++] = 84;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
		
	nMsgLen = PackMsg(send_buf, len, AlivePack, 200);	//数据包转义处理	
	u1_printf("[Alive Pack] -> %d(%d): ", Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	
}

void Create_Data_Message(unsigned int Device_ID)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char Pack[64];
	unsigned int len = 0;
	static u8 seq_no = 0;


	send_buf[len++] = PROTOCOL_CONTROL_ID >> 8;
	send_buf[len++] = PROTOCOL_CONTROL_ID&0xff;
	
	send_buf[len++] = Device_ID >> 24;
	send_buf[len++] = Device_ID >> 16;	
	send_buf[len++] = Device_ID >> 8;
	send_buf[len++] = Device_ID;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 8;

	send_buf[len++] = CMD_REPORT_D;									//命令字
	
	send_buf[len++] = 0x01;	
	send_buf[len++] = 0x00;
	send_buf[len++] = 0x01;	
	send_buf[len++] = 0x00;
	send_buf[len++] = 0xff;	
	send_buf[len++] = 0x04;	
	send_buf[len++] = 0x20;
	send_buf[len++] = 0xD0;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	u1_printf(" [Pack] %d(%d) ->: ", Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	
}

static void COM1Protocol(void)
{
	unsigned char PackBuff[100], DataPack[100];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}

void TaskForDebugCOM(void)
{
	u8 data[32] = "1234567890abcdefg", i, j, Addr, sAddr,  Num = 0, RS485Buf[100], Count = 0;
	unsigned short  nMain10ms = 0, CRCVal;
	unsigned int l_DeviceID;	
	unsigned int  databuf4[64];
	
	SYSTEMCONFIG *p_sys;
	
	if(GetReadInputChannelFlag())
	{
		nMain10ms = GetSystem10msCount();
		
		while(CalculateTime(GetSystem10msCount(), nMain10ms) < 150)
		{
			if(g_UartRxFlag == TRUE && g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_CNT >= 15)
			{
				COM1Protocol();
				Clear_Uart1Buff();
			}		
		}
		SetReadInputChannelFlag(FALSE);
	}
	
	if(g_UartRxFlag == TRUE)
	{				
		if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_CNT >= 15)
		{
			COM1Protocol();
		}
		else
		{
			if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\n RESET CMD\r\n");
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "ClearPluse1", g_USART_RX_CNT) == 0)
			{
				ClearPluseCount1();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "ClearPluse2", g_USART_RX_CNT) == 0)
			{
				ClearPluseCount2();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "ShowGPS", g_USART_RX_CNT) == 0)
			{
				SetShowGPSInfoFlag(TRUE);
			}
			else if(strncmp((char *)g_USART_RX_BUF, "12VON", g_USART_RX_CNT) == 0)
			{
				u1_printf("12VON\r\n");
				SENSOR485_ENABLE();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "12VOFF", g_USART_RX_CNT) == 0)
			{
				u1_printf("12VOFF\r\n");
				SENSOR485_DISABLE();
			}
			
			else if(strncmp((char *)g_USART_RX_BUF, "LORA", g_USART_RX_CNT) == 0)
			{
				LORA_WORK_MODE();
				delay_ms(60);
				u1_printf(" LORA 数据上报测试:\r\n");
				Mod_Test_Communication();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "SensorOff", g_USART_RX_CNT) == 0)
			{
				u1_printf(" 测试模式下关闭传感器循环读取\r\n");
				SetSensorRunFlag(FALSE);
			}
			else if(strncmp((char *)g_USART_RX_BUF, "WORK", g_USART_RX_CNT) == 0)
			{
				u1_printf(" LORA WORK\r\n");
				LORA_WORK_MODE();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "SLEEP", g_USART_RX_CNT) == 0)
			{
				u1_printf(" LORA SLEEP\r\n");
				LORA_SLEEP_MODE();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "INITLORA", g_USART_RX_CNT) == 0)
			{
				u1_printf(" LORA Init\r\n");
				LoraPort_Init();
			}
			
	//		else if(strncmp((char *)g_USART_RX_BUF, "EN485", g_USART_RX_CNT) == 0)
	//		{
	//			u1_printf("Init_485Port\r\n");
	//			Init_485Port();
	//			USART3_Config(9600);
	//		}
	//		else if(strncmp((char *)g_USART_RX_BUF, "DIS485", g_USART_RX_CNT) == 0)
	//		{
	//			u1_printf("Power off 485Port\r\n");
	//			Deinit_485Port();
	//		}
			else if(strncmp((char *)g_USART_RX_BUF, "GPS", g_USART_RX_CNT) == 0)
			{
				GPSPowerOff();
			}	
			else if(strncmp((char *)g_USART_RX_BUF, "Start Charging", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\n Start Charging\r\n");
				SetBQ24650ENFlag(TRUE);	//关闭充电芯片的唯一入口	
				g_PowerDetectFlag = FALSE;
			}	
			else if(strncmp((char *)g_USART_RX_BUF, "Stop Charging", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\n Stop Charging\r\n");
				SetBQ24650ENFlag(FALSE);	//关闭充电芯片的唯一入口	
				g_PowerDetectFlag = FALSE;
			}	

			else if(strncmp((char *)g_USART_RX_BUF, "ModiAddr ", 9) == 0)
			{
				HexStrToByte((char *)&g_USART_RX_BUF[9], &sAddr, 2);
				HexStrToByte((char *)&g_USART_RX_BUF[12], &Addr, 2);
				u1_printf(" Change Addr 0x%02X to 0x%02X\r\n", sAddr, Addr);
				Clear_Uart3Buff();
				Uart_Set_485Addr(SENSOR485_COM, sAddr, Addr);
				Wait485SensorAck();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "Inquire", g_USART_RX_CNT) == 0)
			{
				u1_printf(" Inquire\r\n");
				Clear_Uart3Buff();
				Uart_Inquire_485Addr(SENSOR485_COM);
				Wait485SensorAck();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "TEST MODE", g_USART_RX_CNT) == 0)
			{
					p_sys = GetSystemConfig();
				
				if(GetComTestFlag() == FALSE)
				{
					u1_printf("\r\n Enter TEST MODE\r\n");
					SetComTestFlag(TRUE);
					GPSPowerOff();
					if(p_sys->Type == NETTYPE_LORA)
					{		
						SetTCProtocolForLoraRunFlag(TRUE);
						USART2_Config(115200);	
						LoraPort_Init();
						LORA_WORK_MODE();
					}
					else
					{
						SetTCProtocolRunFlag(FALSE);
						SetTCModuleReadyFlag(FALSE);	
						SIM800CPortInit();
						SIM_PWR_OFF();
						USART2_Config(SIM_BAND_RATE);
						delay_ms(1000);
						
						SIM_PWR_ON();
						
						u1_printf(" PCIE Power On, Usart2 Init Finish\r\n");
					}
	//				SetTCProtocolForLoraRunFlag(TRUE);
					
				}
				else
				{
					u1_printf("\r\n Exit TEST MODE\r\n");
					SetComTestFlag(FALSE);
				}	
			}
	//	    else if(strncmp((char *)g_USART_RX_BUF, "Float4: ", 8) == 0)
	//		{
	//			Num = (g_USART_RX_CNT - 7)/3;
	//			u1_printf("Float:");
	//			for(i=0; i<Num; i++)
	//			{
	//				StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
	//				u1_printf("%02X ", data[i]);
	//			}		
	//			Result = 0;
	//			Result = ((data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3]);			
	//			Int32ToArray(&Arraydata[0], Result);
	//			memcpy(&f_Result, (void *)&Arraydata[0], sizeof(float));
	//			f_Result = encode_float(f_Result);			
	//			u1_printf("Data:%f\r\n", f_Result);
	//		}	
			else if(strncmp((char *)g_USART_RX_BUF, "Alive ", 6) == 0)
			{
				l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
				Create_Alive_Message(l_DeviceID);
			}
			else if(strncmp((char *)g_USART_RX_BUF, "AT", 2) == 0)
			{
				nMain10ms  = GetSystem10msCount();
				u1_printf("Send:%s\r\n", g_USART_RX_BUF);
				u2_printf("%s\r", g_USART_RX_BUF);
				while(CalculateTime(GetSystem10msCount(), nMain10ms) < 150)
				{
					if(g_Uart2RxFlag == TRUE)
					{
						u1_printf("%s\r\n", g_USART2_RX_BUF);
						Clear_Uart2Buff();
						Clear_Uart1Buff();
						return;
					}				
				}
				
				u1_printf("\r\n AT Over Time!\r\n");
			}			
			else if(strncmp((char *)g_USART_RX_BUF, "Data ", 5) == 0)
			{
				l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
				Create_Data_Message(l_DeviceID);
			}
			
			else if(strncmp((char *)g_USART_RX_BUF, "SIM800C", g_USART_RX_CNT) == 0)
			{
				u1_printf("SIM800C ON/OFF\r\n");
				GPIO_ToggleBits(SIM800C_ENABLE_TYPE,SIM800C_ENABLE_PIN);
			}
			else if(strncmp((char *)g_USART_RX_BUF, "485Code ", 8) == 0)
			{
				nMain10ms  = GetSystem10msCount();
				Num = (g_USART_RX_CNT - 7)/3;
				u1_printf("Send 485 Code:");
				for(i=0; i<Num; i++)
				{
					StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
					u1_printf("%02X ", data[i]);
				}		
				
				CRCVal = Calculate_CRC16(data, Num);
				data[Num++] = CRCVal&0xff;
				data[Num++] = CRCVal >> 8;
				
				u1_printf(" %02X %02X", CRCVal&0xff, CRCVal >> 8);
				
				u1_printf("\r\n");
				USART3_DMA_Send(data, Num);
				memset(RS485Buf, 0 ,sizeof(RS485Buf));
				Count = 0;
				while(CalculateTime(GetSystem10msCount(), nMain10ms) < 80)
				{
					if(g_Uart3RxFlag == TRUE)
					{
						g_Uart3RxFlag = FALSE;
						
						memcpy(&RS485Buf[Count], g_USART3_RX_BUF, g_USART3_RX_CNT);
						Count += g_USART3_RX_CNT;
						if(Count > 100)
						{
							break;
						}
						Clear_Uart3Buff();
					}		

					if(Count == (5 + RS485Buf[2]))
					{
						break;
					}					
				}
				for(i=0; i<Count; i++)
				{
					u1_printf("%02X ",RS485Buf[i]);
				}
				u1_printf("\r\n");
			}
			else if(strncmp((char *)g_USART_RX_BUF, "LORACMD ", 8) == 0)
			{
				USART2_Config(9600);	
				LoraPort_Init();
				LORA_SLEEP_MODE();
				delay_ms(100);
				
				nMain10ms  = GetSystem10msCount();
				Num = (g_USART_RX_CNT - 7)/3;
				u1_printf("Send Lora Code:");
				for(i=0; i<Num; i++)
				{
					StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
					u1_printf("%02X ", data[i]);
				}		
				
//				CRCVal = Calculate_CRC16(data, Num);
//				data[Num++] = CRCVal&0xff;
//				data[Num++] = CRCVal >> 8;
				
//				u1_printf(" %02X %02X", CRCVal&0xff, CRCVal >> 8);
				
				u1_printf("\r\n");
				USART2_DMA_Send(data, Num);
				memset(RS485Buf, 0 ,sizeof(RS485Buf));
				Count = 0;
				while(CalculateTime(GetSystem10msCount(), nMain10ms) < 80)
				{
					if(g_Uart2RxFlag == TRUE)
					{
						g_Uart2RxFlag = FALSE;
						
						memcpy(&RS485Buf[Count], g_USART2_RX_BUF, g_USART2_RX_CNT);
						Count += g_USART2_RX_CNT;
						if(Count > 100)
						{
							break;
						}
						Clear_Uart2Buff();
					}						
				}
				for(i=0; i<Count; i++)
				{
					u1_printf("%02X ",RS485Buf[i]);
				}
				u1_printf("\r\n");
			}
			else if(strncmp((char *)g_USART_RX_BUF, "GPRS_Alive", g_USART_RX_CNT) == 0)
			{
				Mod_Send_Alive(GPRS_COM);
			}
			else if(strncmp((char *)g_USART_RX_BUF, "GPRS_Data", g_USART_RX_CNT) == 0)
			{
				Mod_Report_Data(GPRS_COM);
			}	
			else if(strncmp((char *)g_USART_RX_BUF, "EraseAll", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\n Erase all EEPROM data\r\n");
				for(i=0; i< EEPROM_BLOCK_COUNT; i++)
				{
					EEPROM_EraseWords(i);
					u1_printf(" Clear %d Block\r\n", i+1);
				}
			}
			else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
			{
				ShowLogContent();

			}
			else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\n Erase EEPROM Data\r\n");
				EEPROM_EraseWords(9);
				for(i=30; i< EEPROM_BLOCK_COUNT; i++)
				{
					EEPROM_EraseWords(i);
					u1_printf(" Clear %d Block\r\n", i+1);
				}
				
				LogStorePointerInit();	//日志存储区初始化
			}
			else if(strncmp((char *)g_USART_RX_BUF, "EraseCom", g_USART_RX_CNT) == 0)
			{
				ClearCommunicationCount();
				CommunicatinLogInit();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "ShowCom", g_USART_RX_CNT) == 0)
			{
				CommunicatinLogInit();
			}
			else if(strncmp((char *)g_USART_RX_BUF, "READ4", g_USART_RX_CNT) == 0)
			{
				u1_printf("\r\nEEPROM Read4\r\n");
						
				for(j=0; j<64; j++)
				{
					EEPROM_ReadWords(EEPROM_BASE_ADDR + j*64, databuf4, 16); 
					
					for(i=0; i<16; i++)
					{
						u1_printf("%08X ", databuf4[i]);
					}
					u1_printf("\r\n");
				}
				u1_printf("\r\n");
			}
					
		}	
		Clear_Uart1Buff();
	}
	
	if(GetComTestFlag() )	//测试模式开启
	{
		p_sys = GetSystemConfig();
		
		if(p_sys->Type == NETTYPE_2G)
		{
			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("U2 Rec:%s\r\n", g_USART2_RX_BUF);
				Clear_Uart2Buff();
			}		
		}
	}

}










