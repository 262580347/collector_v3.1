

#include "include.h"

UINT16 g_nLedState = 0x0000;		//初始化全部灭
UINT16 g_nLedFlashConfig = 0x0000;	//位为1表示闪烁

//ADC端口对应的灯所在的位置
//UINT8 g_ucLedAdcPos[10] ={ LED_ADC0,LED_ADC1,LED_ADC2,LED_ADC3,LED_ADC4,LED_ADC5,LED_ADC6,LED_ADC7,LED_ADC8,LED_ADC9};

//extern MAP_IN_ITEM *map_in_list[HWL_INPUT_COUNT];
//extern DEVICE_INFO g_device_info;

#define DEBUG_OUTPUT(str)	uart_send_str(DEBUG_UART,str)  //调试信息输出


//LED灯闪烁控制
void SetLedFlash( UINT8 ucPos,UINT8 ucState )
{
	if( ucState )
	{
		g_nLedFlashConfig |= (0x0001<<ucPos);
	}
	else
	{
		g_nLedFlashConfig &= ~(0x0001<<ucPos);
	}
}
UINT16 GetLedFlashState( void )
{
	return g_nLedFlashConfig;
}

//每次只能设置一个灯的状态
void SetLedState( UINT8 ucPos,UINT8 ucState )
{
	if( ucState )
	{
		g_nLedState |= (0x0001<<ucPos);
	}
	else
	{
		g_nLedState &= ~(0x0001<<ucPos);
	}
}

UINT16 GetLedState( void )
{
	return g_nLedState;
}




//=================================================================================
//指示灯由两片74HC595控制，设计思路如下：
//1、初始化时，根据系统配置点亮对应通道的灯
//2、运行时，检查是否有故障（4-20mA或数字信号、电压和脉冲暂时检测不了），正常有故障闪灯
//=================================================================================

//===========================================================
//LED运行灯闪烁控制
//===========================================================
void RunLedFlash( void )
{
	static UINT8 s_ucState = 1;
	if( s_ucState == 1 )
	{
		s_ucState = 0;
		SetLedState(LED_RUN,LED_ON);
	}
	else
	{
		s_ucState = 1;
		SetLedState(LED_RUN,LED_OFF);
	}
}

void RunLedControl( UINT16 nMain100ms )
{
	static UINT16 s_nLast = 0;

	if( GetPowerOnStatus() == 1 )	//电源正常	500MS闪烁一次
	{
		if( GetTimeInterval(nMain100ms,s_nLast) >= 5)
		{
			s_nLast = nMain100ms;
			RunLedFlash();
		}
	}
	else		//市电异常，100MS闪烁一次
	{
		if( GetTimeInterval(nMain100ms,s_nLast) >=1)
		{
			s_nLast = nMain100ms;
			RunLedFlash();
		}
	}
	return;
}


void NetLedFlash( void )
{
	static UINT8 s_ucState = 1;
	if( s_ucState == 1 )
	{
		s_ucState = 0;
		SetLedState(LED_NET,LED_ON);
	}
	else
	{
		s_ucState = 1;
		SetLedState(LED_NET,LED_OFF);
	}
}
//网络连接灯控制
void NetLedControl( UINT16 nMain100ms )
{
	static UINT16 s_nLast = 0;
	nMain100ms = nMain100ms;
	if( GetOnline() == 1 )
	{
		SetLedState( LED_NET, LED_ON );
		SetLedFlash( LED_NET, LED_OFF);	//不闪烁
	}
	else
	{
		SetLedFlash( LED_NET, LED_OFF);	//不闪烁
		if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
		{
			s_nLast = nMain100ms;
			NetLedFlash();
		}	
	}
}

void LanLedFlash( void )
{
	static UINT8 s_ucState = 1;
	if( s_ucState == 1 )
	{
		s_ucState = 0;
		SetLedState(LED_LAN,LED_ON);
	}
	else
	{
		s_ucState = 1;
		SetLedState(LED_LAN,LED_OFF);
	}
}

//LAN灯控制
void LanLedControl( UINT16 nMain100ms )
{
	static UINT16 s_nLast = 0;
	nMain100ms = nMain100ms;
	switch( g_sSystemPrm.m_ucLocalNettype )
	{
		case NETTYPE_ZIGBEE:
			if( GetZigbeeReadyFlag() == 1 )
			{
				SetLedState( LED_LAN, LED_ON );
				SetLedFlash(LED_LAN,LED_OFF);	//不闪烁
			}
			else
			{
				SetLedFlash( LED_LAN, LED_OFF);	//不闪烁
				if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
				{
					s_nLast = nMain100ms;
					LanLedFlash();
				}
			}
			break;
		
		case NETTYPE_XBEE:
			if( GetXbeeReadyFlag() == 1 )
			{
				SetLedState( LED_LAN, LED_ON );
				SetLedFlash(LED_LAN,LED_OFF);	//不闪烁
			}
			else
			{
				SetLedFlash( LED_LAN, LED_OFF);	//不闪烁
				if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
				{
					s_nLast = nMain100ms;
					LanLedFlash();
				}
			}
			break;
			
		case NETTYPE_LORA:
			if( GetLoraReadyFlag() == 1 )
			{
				SetLedState( LED_LAN, LED_ON );
				SetLedFlash(LED_LAN,LED_OFF);	//不闪烁
			}
			else
			{
				SetLedFlash( LED_LAN, LED_OFF);	//不闪烁
				if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
				{
					s_nLast = nMain100ms;
					LanLedFlash();
				}
			}
			break;
		
		case NETTYPE_GPRS:
			if( GetGprsReadyFlag() == 1 )
			{
				SetLedState( LED_LAN, LED_ON );
				SetLedFlash(LED_LAN,LED_OFF);	//不闪烁
			}
			else
			{
				SetLedFlash( LED_LAN, LED_OFF);	//不闪烁
				if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
				{
					s_nLast = nMain100ms;
					LanLedFlash();
				}
			}
			break;
		
		case NETTYPE_SIM800C:
			if( GetSim800cReadyFlag() == 1 )
			{
				SetLedState( LED_LAN, LED_ON );
				SetLedFlash(LED_LAN,LED_OFF);	//不闪烁
			}
			else
			{
				SetLedFlash( LED_LAN, LED_OFF);	//不闪烁
				if( GetTimeInterval(nMain100ms,s_nLast) >= 2)
				{
					s_nLast = nMain100ms;
					LanLedFlash();
				}
			}
			break;
			
	}
}

//指示灯控制
void LedControlProc( UINT16 nMain100ms )
{
//	static UINT16 s_nLast = 0;
	static UINT16 s_nLastLedState = 0;
	static UINT8 s_ucFirst = 1;
//	UINT16 nTemp = 0;
//	static UINT8 s_ucFlag = 0;
	
//	char str[100];
	
//	UINT8 i=0;
	
	
//	MAP_IN_ITEM *item;			//输入配置
	
	
	//初始化，根据配置亮灯
	if( s_ucFirst )
	{
		s_ucFirst = 0;
		LedPortInit();
		UpdateLedState( 0xFFFF );	//初始化为亮
		g_nLedState = 0x0000;
		return;
	}
	
	//g_nLedState = 0;	//清零后，再重置指示灯的状态
	
	RunLedControl( nMain100ms );//运行指示灯控制

	LanLedControl( nMain100ms);	//组网指示灯控制

	NetLedControl( nMain100ms );//联网指示灯控制

	
//	//各个通道的灯===================
//	for( i=0;i< HWL_INPUT_COUNT; i++)
//	{
//		item = map_in_list[i];
//		if( item == NULL )	//通道没有配置
//		{
//			continue;
//		}
//		
//		switch(item->data_id)
//		{
//			//特殊处理类型，in_param[0]的值0、1用于数字信号，2--9用于ADC
//			case KQW_ID: 		//空气温度	
//			case KQS_ID:	 	//空气湿度	
//			case TRW_ID:		//土壤温度
//				if( item->in_param[0] == 0 )
//					SetLedState(LED_D1, LED_ON );
//				else if ( item->in_param[0] == 1 )
//					SetLedState( LED_D2,LED_ON );
//				else if( item->in_param[0] < 10 )
//					SetLedState(g_ucLedAdcPos[ item->in_param[0] - 2],LED_ON );	
//				break;

//			case JYL_ID:	//雨量
//			case LL_ID:		//流量
//				if( item->in_param[0] == 0 )
//					SetLedState(LED_P1, LED_ON );
//				else if ( item->in_param[0] == 1 )
//					SetLedState( LED_P2,LED_ON );
//				break;
//			
//			default:
//				SetLedState(g_ucLedAdcPos[ item->in_param[0] ],LED_ON );
//				break;
//		}
//	}
	
//	//LED闪烁控制--故障的通道闪烁
//	if( GetTimeInterval(nMain100ms,s_nLast) >= 2 )
//	{
//		s_nLast = nMain100ms;
//		if(s_ucFlag == 1)
//		{
////			sprintf(str,"g_nState = 0x%04X,g_nLedFlash = 0x%04X,s_ucFlag =%d\r\n",g_nLedState,g_nLedFlashConfig,s_ucFlag);
////			DEBUG_OUTPUT(str);
//			s_ucFlag = 0;
//			UpdateLedState( GetLedState() );	//亮
//		}
//		else
//		{
////			sprintf(str,"g_nState = 0x%04X,g_nLedFlash = 0x%04X,s_ucFlag =%d\r\n",g_nLedState,g_nLedFlashConfig,s_ucFlag);
////			DEBUG_OUTPUT(str);			
//			s_ucFlag = 1;
//			nTemp = ~g_nLedFlashConfig;
//			UpdateLedState( GetLedState() & nTemp );	//故障的通道灭
//		}
//	}

	//更新LED指示灯状态
	if( s_nLastLedState != GetLedState() )	//LED灯状态更新
	{
		s_nLastLedState = GetLedState();
		UpdateLedState( s_nLastLedState );
	}
}



//void LedTest( UINT16 nMain100ms )
//{
//	//LED指示灯状态闪烁
//	if( GetTimeInterval(nMain100ms,s_nLast) >= 1 )
//	{
//		s_nLast = nMain100ms;
//		if(s_ucFlag == 1)
//		{
//			sprintf(str,"g_nState=0x%04X\r\n",g_nLedState);
//			DEBUG_OUTPUT(str);
//			s_ucFlag = 0;
//			UpdateLedState( 0 );
//		}
//		else
//		{
//			sprintf(str,"g_nState=0x%04X\r\n",g_nLedState);
//			DEBUG_OUTPUT(str);			
//			s_ucFlag = 1;
//			UpdateLedState( GetLedState() );
//		}
//	}

//	//测试一个灯一个灯亮
//	if( GetTimeInterval(nMain100ms,s_nLast) > 2 )
//	{
//		s_nLast = nMain100ms;
//		g_nLedState = 0;
//		SetLedState( s_ucFlag,LED_ON);
//		UpdateLedState( g_nLedState );
//		
//		sprintf(str,"g_nState=0x%04X,s_ucFlag=%d\r\n",g_nLedState,s_ucFlag);
//		DEBUG_OUTPUT(str);		
//		
//		s_ucFlag++;
//		if( s_ucFlag > 16 )
//		{
//			s_ucFlag = 0;
//		}	
//	}
//}




























