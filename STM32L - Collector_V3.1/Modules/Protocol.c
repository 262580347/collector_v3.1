#include "Protocol.h"
#include "main.h"

void Mod_Send_Ack(USART_TypeDef* USARTx, CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char Pack[128];
	unsigned int len = 0;
	SYSTEMCONFIG *p_sys;
	
	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	p_sys = GetSystemConfig();			
	if(p_sys->LowpowerFlag == 0)
	{
		send_buf[len++] = ((hdr->device_id >> 24) | 0x10000000);//低功耗设备
	}
	else if(p_sys->LowpowerFlag == 1)
	{
		send_buf[len++] = ((hdr->device_id >> 24));	//非低功耗设备
	}
		
	
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no ;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	USART2_DMA_Send(Pack, nMsgLen);	

	u1_printf("\r\n [ACK] from %d(%d) ->: ", (hdr->device_id)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");		
}

void Mod_Test_Communication(void)
{
	u16 nMsgLen;
	static u16 seq;
	unsigned int send_len;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256];

	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16;

	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);				//通信版本号

	hdr->device_id = swap_dword(p_sys->Device_ID);	 

	hdr->dir = 0;												  	//方向
	hdr->seq_no = swap_word(seq);							  		//包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 0;

	val_u16 = swap_word(25);		//通道25
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);	
	
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);

	count++;	

	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,250);

	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, Packet, nMsgLen);

	//发送消息
	u1_printf("\r\n [%02d:%02d:%02d][%d][Test] -> %d(%d): ",  RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, (p_sys->Device_ID)&0xfffffff,  p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Packet[i]);
	}
	u1_printf("\r\n");	
}

unsigned int Mod_Report_Data(USART_TypeDef* USARTx)
{
	u16 nMsgLen;
	static u16 seq;
	unsigned int send_len, OverTime = 0;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256], WaitTime;
	DATA_INFO data_info;
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16, ch;
	float val_float;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);				//通信版本号
	if(p_sys->Type == NETTYPE_LORA)
	{
		hdr->device_id = swap_dword(p_sys->Device_ID);	 
	}
	else 
	{
		if(p_sys->LowpowerFlag == 0)
		{
			hdr->device_id = swap_dword((p_sys->Device_ID) | 0x10000000) ;//低功耗设备
		}
		else if(p_sys->LowpowerFlag == 1)
		{
			hdr->device_id = swap_dword((p_sys->Device_ID)) ;//非低功耗设备
		}
			 	//设备号,设备号的高四位的最低位作为低功耗设备标记，2019.2.13
	}
	hdr->dir = 0;												  	//方向
	hdr->seq_no = swap_word(seq);							  		//包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 0;
	
	u1_printf("\r\n----Report Data----\r\n");
	
	for (i=0;i<HWL_INPUT_COUNT;i++)
	{
		if (hwl_get_input_value(i, &data_info))
		{
			ch = (i+1);
			val_u16 = swap_word(ch);		//通道
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			val_u16 = swap_word(data_info.data_id);//类型
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			(*ptr) = MARK_FLOAT;	//数据标识
			ptr++;
			val_float = encode_float(data_info.value);	//数据
			memcpy(ptr, (void *)&val_float, sizeof(float));
			ptr += sizeof(float);
			count++;
		}
	}	
	

	if(GetGPSRunFlag() || GetGPSDataFlag())
	{	
		if(GetLongitude() !=0 && GetLatitude() != 0)
		{
			val_u16 = swap_word(CH_LONGITUDE);		//通道
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			val_u16 = swap_word(LONGITUDE_ID);//类型
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			(*ptr) = MARK_FLOAT;	//数据标识
			ptr++;
			val_float = encode_float(GetLongitude());	//数据
			memcpy(ptr, (void *)&val_float, sizeof(float));
			ptr += sizeof(float);

			count++;
			val_u16 = swap_word(CH_LATITUDE);		//通道
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			val_u16 = swap_word(LATITUDE_ID);//类型
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			(*ptr) = MARK_FLOAT;	//数据标识
			ptr++;
			val_float = encode_float(GetLatitude());	//数据
			memcpy(ptr, (void *)&val_float, sizeof(float));
			ptr += sizeof(float);
			
			count++;
		}		
	}
	
	if(p_sys->Type != NETTYPE_LORA)
	{
		val_u16 = swap_word(CH_CSQ);
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		val_u16 = swap_word(CSQ_ID);//类型
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		(*ptr) = MARK_FLOAT;	//数据标识
		ptr++;
		val_float = encode_float(GetCSQValue());	//数据
		memcpy(ptr, (void *)&val_float, sizeof(float));
		ptr += sizeof(float);
		count++;	
		
		val_u16 = swap_word(CH_LINKTIME);
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		val_u16 = swap_word(LINKTIME_ID);//类型
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		(*ptr) = MARK_FLOAT;	//数据标识
		ptr++;
		val_float = encode_float(GetLinkNetTime()/100.0);	//数据
		memcpy(ptr, (void *)&val_float, sizeof(float));
		ptr += sizeof(float);
		count++;	
	}
	
	val_u16 = swap_word(CH_DCPOWERVOL);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);		
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetDCPowerVol()/1000.0);	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	count++;	
	
	val_u16 = swap_word(CH_BATVOL);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);		
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	count++;	
		
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,250);
	if(USARTx == USART1)
	{
		Clear_Uart1Buff();
		delay_ms(1);
		Uart_Send_Data(USARTx, Packet, nMsgLen);
//		u1_printf(Packet);
		return nMsgLen;
	}
	if(p_sys->Type == NETTYPE_LORA)
	{
		OverTime = 0;
		while(1)	//奇数秒上报
		{
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
			WaitTime = RTC_TimeStructure.RTC_Seconds%10;
			if(WaitTime < 3)	//0~2s区间为采集器上报时间
			{
				break;
			}
			OverTime++;
			if(OverTime >= 100000)
			{
//				u1_printf("\r\n Over Time!\r\n");
				break;
			}
		}
		Lora_Send_Data(p_sys->Gateway, p_sys->Channel, Packet, nMsgLen);
	}
	else 
	{
		USART2_DMA_Send(Packet, nMsgLen);
	}
	
	//发送消息
	u1_printf("\r\n [%02d:%02d:%02d][%d][Data] -> %d(%d): ",  RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, (p_sys->Device_ID)&0xfffffff,  p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Packet[i]);
	}
	u1_printf("\r\n");	
	
	return nMsgLen;	
}


void Mod_Send_Alive(USART_TypeDef* USARTx)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);							//通信协议版本号
		
	if(p_sys->LowpowerFlag == 0)
	{
		hdr->device_id = swap_dword((p_sys->Device_ID) | 0x10000000);//低功耗设备
	}
	else if(p_sys->LowpowerFlag == 1)
	{
		hdr->device_id = swap_dword((p_sys->Device_ID));//非低功耗设备
	}
	
			//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	send_buf[len++] = 84;	//电池电压
	
	Int32ToArray(&send_buf[len], p_sys->Device_ID);
	len +=4;
	
	send_buf[len++] = 20;	//信号强度

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AlivePack, 100);		//数据包转义处理	
	u1_printf("\r\n [Alive] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	

	USART2_DMA_Send(AlivePack, nMsgLen);	
}
//主动上报系统配置信息
unsigned int ReportSystemConfig(void)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	static u16 seq_no = 0;
	u16 nMsgLen, i;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG  *com_sys_cfg;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	send_len = sizeof(CLOUD_HDR);
	
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);	
	if(p_sys->LowpowerFlag == 0)
	{
		hdr->device_id = swap_dword((p_sys->Device_ID) | 0x10000000);//低功耗设备
	}
	else if(p_sys->LowpowerFlag == 1)
	{
		hdr->device_id = swap_dword((p_sys->Device_ID));//非低功耗设备
	}
	
			//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);							//信息域长度
	
	hdr->cmd = CMD_GET_CONFIG;									//命令字
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	if(sys_cfg->LowpowerFlag == 0)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);	//低功耗设备
	}
	else if(sys_cfg->LowpowerFlag == 1)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);	//非低功耗设备
	}
	
	
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_LowpowerFlag = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);		
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息

	USART2_DMA_Send(Packet, nMsgLen);	

	u1_printf("\r\n [Report Config] -> %d (%d):", (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	
	u1_printf("\r\n");	
	return 1;
}
//读取设备通信配置信息 --0x04 =========================================
unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG  *com_sys_cfg;
		
	memcpy(send_buf, pMsg, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->protocol = swap_word(hdr->protocol);	
	hdr->device_id = swap_dword(hdr->device_id);
	hdr->seq_no = swap_word(hdr->seq_no);	
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	if(sys_cfg->LowpowerFlag == 0)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);	//低功耗设备
	}
	else if(sys_cfg->LowpowerFlag == 1)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);	//非低功耗设备
	}
	
	
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_LowpowerFlag = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);		
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息

//	if(USARTx == LORA_COM)
//		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
//	else 
//		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		USART2_DMA_Send(Packet, nMsgLen);	
	}
	u1_printf("[Mod][Config] -> %d (%d):", (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	
	u1_printf("\r\n");	
	return 1;
}
//写入通道配置信息 0x05 ===============================================
unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	
	SYSTEMCONFIG new_sys_cfg;
	SYSTEMCONFIG *sys_cfg, *p_sys;
	COM_SYSTEM_CFG *com_sys_cfg;
	
	uint16 val_u16;
	unsigned int val_u32;
	unsigned int payload_len;

	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	ptr = &data[sizeof(CLOUD_HDR)];
	hdr = (CLOUD_HDR *)data;
	
	payload_len = swap_word(hdr->payload_len);
	
	sys_cfg = GetSystemConfig();
	memcpy(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3);
	
	while(payload_len)
	{
		switch((*ptr))
		{
			case CFG_ID_1:	  	//系统信息分组ID
				ptr++;
				payload_len--;
				val_u32 = *(unsigned int *)ptr;
				new_sys_cfg.Device_ID = (swap_dword(val_u32)) & 0xfffffff ;
				ptr += sizeof(unsigned int);
				new_sys_cfg.Type = (*ptr);
				ptr++;
				payload_len-=5;

				u1_printf("[Set]:Device=%u,Net type=%d\n",new_sys_cfg.Device_ID,new_sys_cfg.Type );
				
				if((new_sys_cfg.Type != NETTYPE_LORA) && (new_sys_cfg.Type != NETTYPE_2G) && (new_sys_cfg.Type != NETTYPE_GPRS) )
				{
					return 0;
				}
				
				break;
			case CFG_ID_2: 	//Zigbee信息分组ID
				ptr++;
				payload_len--;
			
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Net = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Node = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gateway = swap_word(val_u16);
				ptr += sizeof(uint16);
				new_sys_cfg.Channel = (*ptr);
				ptr++;
				new_sys_cfg.LowpowerFlag = (*ptr);
				ptr++;
				payload_len-=8;

				u1_printf("[Set]:Lora Net num=%u,Node=%d,Gateway=%d,Channel=%d,Go to Sleep=%d\r\n"
						,new_sys_cfg.Net
						,new_sys_cfg.Node
						,new_sys_cfg.Gateway
						,new_sys_cfg.Channel
						,new_sys_cfg.LowpowerFlag
						 );
				if(new_sys_cfg.Channel > 32)
				{
					u1_printf("\r\n Channel Err\r\n");
					return 0;
				}
				if(new_sys_cfg.Gateway != new_sys_cfg.Net)
				{
					u1_printf("\r\n Net Err\r\n");
					return 0;
				}
				
				break;
			case CFG_ID_3:		//GPRS分组信息ID
				ptr++;
				payload_len--;
			
				memcpy(new_sys_cfg.Gprs_ServerIP, ptr, 4);
				ptr += 4;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gprs_Port = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=6;

				u1_printf("%u.%u.%u.%u,Port=%d\n"
						,(new_sys_cfg.Gprs_ServerIP[0])
						,(new_sys_cfg.Gprs_ServerIP[1])
						,(new_sys_cfg.Gprs_ServerIP[2])
						,(new_sys_cfg.Gprs_ServerIP[3])
						,new_sys_cfg.Gprs_Port
						 );
				break;
			case CFG_ID_4: 	//通信参数分组ID
				ptr++;
				payload_len--;
			
				new_sys_cfg.Retry_Times = (*ptr);
				ptr++;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Heart_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Data_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.State_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=7;

				u1_printf("Count=%u.Heart=%u.Data=%u.Wait=%u\n"
						,new_sys_cfg.Retry_Times
						,new_sys_cfg.Heart_interval
						,new_sys_cfg.Data_interval
						,new_sys_cfg.State_interval
						 );
						 
				if(new_sys_cfg.Heart_interval > 30000)
				{
					return 0;
				}
				
				if(new_sys_cfg.Data_interval > 30000)
				{
					return 0;
				}
				
				if(new_sys_cfg.State_interval > 30000)
				{
					return 0;
				}
				
				break;
			default:
				ptr++;
				payload_len--;
				break;
		}
	}
	
	if (memcmp(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3))
	{
		u1_printf("\r\n Updata Config...");
		new_sys_cfg.AutoResetFlag = sys_cfg->AutoResetFlag;
		new_sys_cfg.AutoResetTime = sys_cfg->AutoResetTime;
		Set_System_Config(&new_sys_cfg);
	}
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);
	send_len = sizeof(CLOUD_HDR);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	
	p_sys = GetSystemConfig();			
	if(p_sys->LowpowerFlag == 0)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);	//低功耗设备
	}
	else if(p_sys->LowpowerFlag == 1)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);	//非低功耗设备
	}

	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_LowpowerFlag = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);	
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息
	u1_printf("[Mod][Updata Config] -> %d (%d):",  (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", send_buf[i]);
	}	
	u1_printf("\r\n");
	
//	if(USARTx == LORA_COM)
//		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
//	else 
//		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		USART2_DMA_Send(Packet, nMsgLen);	
	}
	
	return 1;
}




