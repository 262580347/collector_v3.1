#include "SensorManage.h"
#include "main.h"

#define		ERROR_485VALUE		0XEEEEEEEE

MAP_IN_ITEM  *map_in_list[HWL_INPUT_COUNT];	

static unsigned char s_RJYSensorFlag = FALSE;
static unsigned char s_RJYSensorAddr = 0;

static unsigned char s_ChlorophyllSensorFlag = FALSE;
static unsigned char s_ChlorophyllSensorAddr = 0;

unsigned char GetRJYSensorFlag(unsigned char *Addr)
{
	*Addr = s_RJYSensorAddr;
	return s_RJYSensorFlag;
}

unsigned char GetChlorophyllSensorFlag(unsigned char *Addr)
{
	*Addr = s_ChlorophyllSensorAddr;
	return s_ChlorophyllSensorFlag;
}

unsigned char ShowSensorName(unsigned char SensorID, unsigned char isTrue, float SensorVal)
{
	u32 error_mark = ERROR_485VALUE;
	
	switch(SensorID)
	{
		case KQW_ID: 		//Air Temperature
			u1_printf("\r\n 空温度");

		break;
		
		case KQS_ID:	 	//Air Humidity
			u1_printf("\r\n 空湿度");

		break;

		case TRW_ID:		//Soil Temperature
			u1_printf("\r\n 土温度");

			break;
		case TRS_ID:		//Soil Humidity
			u1_printf("\r\n 土湿度");

			break;
		case SOIL_EC_ID:		//Soil Electric
			u1_printf("\r\n 土电导率");

			break;
		case SOIL_YF_ID:		//Soil Salinity
			u1_printf("\r\n 土盐分");

			break;
		case GZD_ID:		//Illuminance
			u1_printf("\r\n 光照度");

		break;
		case CO2_ID:		//Carbon Dioxide
			u1_printf("\r\n CO2");

		break;
		case FS_ID:			  //Wind Speed
			u1_printf("\r\n 风速");

			break;
		case FX_ID:			  //Wind Direction
			u1_printf("\r\n 风向");

			break;
		case AIR_PRESS_ID:	  //Atmospheric Pressure
			u1_printf("\r\n 大气压力");

			break;
		case PH_ID:			  //PH Value
			u1_printf("\r\n PH值");

			break;
		case SYL_ID:			  //Water Pressure
			u1_printf("\r\n 水压力");

			break;
		case YW_ID:			  //Liquid Level
			u1_printf("\r\n 液位");

			break;
		case YMSD_ID:			  //Leaf Wetness
			u1_printf("\r\n 叶面湿度");

			break;
		case NOISE_ID:			  //Noise Decibel
			u1_printf("\r\n 噪声");

			break;
		case H2S_ID:			  //H2S
			u1_printf("\r\n H2S");

			break;
		case NH3_ID:			  //NH3
			u1_printf("\r\n NH3");

			break;
		case PHOTOSYNTHETIC_ACTIVE_RADIATION_ID:			  //Photosynthetically Active Radiation
			u1_printf("\r\n 光合有效辐射");

			break;
		case EVAPORATION_ID:			  //Evaporation
			u1_printf("\r\n 蒸发量");

			break;
		case SOLAR_RADIATION_ID:			  //Solar Radiation
			u1_printf("\r\n 太阳辐射");

			break;
		case AIR_ION:			  //Air Ion
			u1_printf("\r\n 负氧离子");

			break;	
		case DEW_POINT:			  //Dew Point
			u1_printf("\r\n 露点");

			break;	
		case O2_ID:			  //O2
			u1_printf("\r\n O2");

			break;
		case CH4_ID:			  //CH4
			u1_printf("\r\n CH4");

			break;
		case CO_ID:			  //CO
			u1_printf("\r\n CO");

			break;
		case NO2_ID:			  //NO2
			u1_printf("\r\n NO2");

			break;
		case SO2_ID:			  //SO2
			u1_printf("\r\n SO2");

			break;
		case SMOKE_ID:			  //Smoke
			u1_printf("\r\n 粉尘");

			break;
		case HCL_ID:			  //HCL
			u1_printf("\r\n HCL");

			break;
		case RJY_ID:			  //Dissolved Oxygen
			u1_printf("\r\n 降雨量");
			break;
		case COD_ID:			  //COD
			u1_printf("\r\n COD");

			break;
		case NH3_NH4_ID:			  //NH3 NH4
			u1_printf("\r\n NH3 NH4");

			break;
		case DDL_ID:			  //Water Electric
			u1_printf("\r\n 水电导率");

			break;
		case H2O_NH3_ID:			  //Water NH3 NH4
			u1_printf("\r\n Water NH3 NH4");

			break;
		case H2O_TURBIDITY_ID:			  //Turbidimeter
			u1_printf("\r\n 浊度");

			break;
		case H2O_CL2_ID:			  //Water CL2
			u1_printf("\r\n Water CL2");

			break;
		case SOIL_M_ID:			  //Soil Tension
			u1_printf("\r\n 土张力");

			break;
		case JYL_ID:			  //RainFall
			u1_printf("\r\n 降雨量");
		
			break;
	
		case LL_ID:			  //Flow
			u1_printf("\r\n 流量");

			break;
		
		case ORP_POTENTIOMETER_ID:			  
			u1_printf("\r\n ORP氧化还原");

			break;	
		case CHLOROPHYLL_ID:			 
			u1_printf("\r\n 叶绿素");

			break;	
		case LONGITUDE_ID:			  
			u1_printf("\r\n 经度");

			break;	
		case LATITUDE_ID:			
			u1_printf("\r\n 纬度");

			break;	
		case SOIL_WATER_POTENTIAL:
			u1_printf("\r\n 土水势");		
			break;
		
		default:
			u1_printf(" No id\r\n");
		break;
	}
	
	if(isTrue)
	{
		if(SensorVal == ERROR_485VALUE)
		{	
			u1_printf(" Err\r\n");	
			return FALSE;	
		}
		else if(memcmp((uint8 *)&SensorVal, (uint8 *)&error_mark, sizeof(uint32)) == 0)
		{
			u1_printf(" Err\r\n");	
			return FALSE;
		}
		else
		{
			u1_printf(":%2.3f\r\n", SensorVal);
			return TRUE;
		}
	}
	return TRUE;
}

static void SetWaitingTime(unsigned char Time)
{
	if(g_ReportDataWaitingTime < Time)
	{
		g_ReportDataWaitingTime = Time;
	}
}

unsigned char hwl_input_init(unsigned int input_ch, unsigned char Display)
{
	MAP_IN_ITEM *item;
	
	item = get_map_in_item(input_ch);
	map_in_list[input_ch] = item;
	
	if (item == NULL) 
	{
		return 0;	
	}
	else
	{
		if(Display)
		{
			ShowSensorName(item->data_id, FALSE, 0);
			switch(item->data_id)
			{
				case KQW_ID: 		//Air Temperature
					SetWaitingTime(5);
					break;			
				case KQS_ID:	 	//Air Air Humidity
					SetWaitingTime(5);
					break;
				case TRW_ID:		//Soil Temperature
					SetWaitingTime(10);
					break;
				case TRS_ID:		//Soil Humidity
					SetWaitingTime(5);
					break;
				case SOIL_EC_ID:		//Soil Electric
					SetWaitingTime(17);
					break;
				case SOIL_YF_ID:		//Soil Salinity
					SetWaitingTime(13);
					break;
				case SOIL_WATER_POTENTIAL:
					SetWaitingTime(10);
					break;
				case GZD_ID:		//Illuminance
					SetWaitingTime(5);
					break;
				case CO2_ID:		//Carbon Dioxide
					SetWaitingTime(30);
					SetForcedChargeFlag(TRUE);
					break;
				case FS_ID:			  //Wind Speed
					SetWaitingTime(5);
					break;
				case FX_ID:			  //Wind Direction
					SetWaitingTime(5);
					break;
				case AIR_PRESS_ID:	  //Atmospheric Pressure
					SetWaitingTime(5);
					break;
				case PH_ID:			  //PH Value
					SetWaitingTime(15);
					break;
				case SYL_ID:			  //Water Pressure
					SetWaitingTime(5);
					break;
				case YW_ID:			  //Liquid Level			
					SetWaitingTime(5);
					break;
				case YMSD_ID:			  //Leaf Wetness
					SetWaitingTime(5);
					break;
				case NOISE_ID:			  //Noise Decibel
					SetWaitingTime(5);
					SetForcedChargeFlag(TRUE);
					break;
				case H2S_ID:			  //H2S
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case NH3_ID:			  //NH3
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case PHOTOSYNTHETIC_ACTIVE_RADIATION_ID:			  //Photosynthetically Active Radiation
					SetWaitingTime(5);
					break;
				case EVAPORATION_ID:			  //Evaporation
					SetWaitingTime(5);
					break;
				case SOLAR_RADIATION_ID:			  //Solar Radiation
					SetWaitingTime(5);
					break;
					
				case AIR_ION:			  //Air Ion
					SetWaitingTime(30);
					SetForcedChargeFlag(TRUE);
				break;	
				case DEW_POINT:			  //Dew Point
					SetWaitingTime(5);

				break;	
				case O2_ID:			  //O2
					SetWaitingTime(10);
					SetForcedChargeFlag(TRUE);
					break;
				case CH4_ID:			  //CH4
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case CO_ID:			  //CO
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case NO2_ID:			  //NO2
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case SO2_ID:			  //SO2
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case SMOKE_ID:			  //Smoke
					SetWaitingTime(30);
					SetForcedChargeFlag(TRUE);
					break;
				case HCL_ID:			  //HCL
					SetWaitingTime(2);
					break;
				case RJY_ID:			  //Dissolved Oxygen
					s_RJYSensorAddr = item->in_param[2];
					s_RJYSensorFlag = TRUE;
					SetWaitingTime(5);
					SetForcedChargeFlag(TRUE);
					break;
				case COD_ID:			  //COD
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case NH3_NH4_ID:			  //NH3 NH4
					SetWaitingTime(20);
					SetForcedChargeFlag(TRUE);
					break;
				case DDL_ID:			  //Water Electric
					SetWaitingTime(17);
					break;
				case H2O_NH3_ID:			  //水质NH3 NH4
					SetWaitingTime(15);
					SetForcedChargeFlag(TRUE);
					break;
				case H2O_TURBIDITY_ID:			  //Turbidimeter
					SetWaitingTime(15);
					SetForcedChargeFlag(TRUE);
					break;
				case H2O_CL2_ID:			  //Water CL2
					SetWaitingTime(15);
					SetForcedChargeFlag(TRUE);
					break;
				case SOIL_M_ID:			  //Soil Tension
					SetWaitingTime(5);
					break;
				
				case ORP_POTENTIOMETER_ID:		//ORP
					SetWaitingTime(30);
					SetForcedChargeFlag(TRUE);
					break;
				
				case CHLOROPHYLL_ID:			  //Chlorophyll
					SetWaitingTime(30);
					s_ChlorophyllSensorAddr = item->in_param[2];
					s_ChlorophyllSensorFlag = TRUE;
					SetForcedChargeFlag(TRUE);
					break;
				case JYL_ID:			  //RainFall
					InitPluseCount();
					if(item->in_param[0] == 0)
					{
						Init_PulseSensorPort();
						g_PluseChannel_1_Flag = TRUE;
						u1_printf("1");
					}
					else if(item->in_param[0] == 1)
					{
						Init_Pulse2SensorPort();						
						g_PluseChannel_2_Flag = TRUE;
						u1_printf("2");
					}
					else if(item->in_param[0] == 3)
					{
						u1_printf("RS485");
					}
					break;	
				case LL_ID:			  //Flow
					InitPluseCount();
					if(item->in_param[0] == 0)
					{
						Init_PulseSensorPort();
						g_PluseChannel_1_Flag = TRUE;
						u1_printf("1");
					}
					else if(item->in_param[0] == 1)
					{
						Init_Pulse2SensorPort();
						g_PluseChannel_2_Flag = TRUE;
						u1_printf("2");
					}
					else if(item->in_param[0] == 2)
					{
						Init_PulseSensorPort();					
						Init_Pulse2SensorPort();
						g_PluseChannel_1_Flag = TRUE;
						g_PluseChannel_2_Flag = TRUE;
						u1_printf("(Pluse together)");
						SetUnionPluse(TRUE);
					}
					
				
					break;	
			}
							
			u1_printf(" (%d):类型:0x%02X,RS485地址:0x%02X,寄存器地址:0x%04X,比例系数:",input_ch+1 , item->data_id, item->in_param[2], (item->in_param[3]<<8) + item->in_param[4]);
			switch(item->in_param[5])
			{
				case 0:
					u1_printf("0");
				break;
				case 1:
					u1_printf("0.001");
				break;
				case 2:
					u1_printf("0.01");
				break;
				case 3:
					u1_printf("0.1");
				break;
				case 4:
					u1_printf("0.5");
				break;
				case 5:
					u1_printf("1");
				break;
				case 6:
					u1_printf("2");
				break;
				case 7:
					u1_printf("5");
				break;
				case 8:
					u1_printf("10");
				break;
				case 9:
					u1_printf("20");
				break;
				case 10:
					u1_printf("50");
				break;
				case 11:
					u1_printf("100");
				break;
				case 12:
					u1_printf("500");
				break;
				case 13:
					u1_printf("1000");
				break;
				default:
					u1_printf("Err");
				break;
			}
			if(item->in_param[6])
			{
				u1_printf(",功能码:%02X", item->in_param[6]);
			}
			if(item->in_param[7])
			{
				u1_printf(",读取数:%02X", item->in_param[7]);
			}
			if(item->compensation)	//补偿
			{
				u1_printf(",补偿:%.2f", item->compensation);
			}
			
			u1_printf("\r\n");
		}
	
	}
		return 1;
}


//根据输入配置信息从具体绑定的硬件上获取数据(如果能正确映射,否则通过函数返回结果说明获取失败)
unsigned int hwl_get_input_value(unsigned int input_ch, DATA_INFO *data_info)
{
	MAP_IN_ITEM *item;
	
	float result = ERROR_VALUE;
	uint32 error_mark = 0xEEEEEEEE;
	RANGE_SPEC *range_spec;
	
	if (input_ch >= HWL_INPUT_COUNT) return 0;
	
	item = get_map_in_item(input_ch);

	if (item == NULL) return 0;

	if( item->data_id == TRS_ID )	//Soil Humidity数据处理
	{
		range_spec = (RANGE_SPEC *)((unsigned int)item + sizeof(MAP_IN_ITEM) + sizeof(SOILWATER_SPEC));  //Soil Humidity的量程范围
	}
	else
	{
		range_spec = (RANGE_SPEC *)((unsigned int)item + sizeof(MAP_IN_ITEM));	//通用量程范围
	}
	
	result = GetSensorData(input_ch);
	
	if(ShowSensorName(item->data_id, TRUE, result))
	{		
	//	result = result + item->compensation;	//修正值
	}
				
	if(memcmp((uint8 *)&result, (uint8 *)&error_mark, sizeof(uint32)) != 0)
	{
		if(range_spec->high_limit || range_spec->low_limit)	//若设定了上下限，则最终数据不会超出其范围，若未设置，即为读数。
		{
			if(result > range_spec->high_limit)
			{
				result = range_spec->high_limit;
				u1_printf(" Data upper limit%.2f\r\n", range_spec->high_limit);
			}
			else if(result < range_spec->low_limit)
			{
				result = range_spec->low_limit;
				u1_printf(" Data lower limit%.2f\r\n", range_spec->low_limit);
			}
		}
	}
	
	data_info->value = result;	
	data_info->data_id = item->data_id;
	data_info->attr_ptr = (void *)map_in_list[input_ch];
	
	return 1;
}

















