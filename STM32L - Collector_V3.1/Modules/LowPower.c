/**********************************
说明：低功耗模式下的进入与退出管理
	  主要重新对时钟、外设、端口进行配置
	  清除对应标志位
	  
	  1.修复了重发触发中断的问题
	  2.同时兼容了多路外部中断唤醒
作者：关宇晟
版本：V2017.11.20
***********************************/
#include "main.h"
#include "LowPower.h"
 
unsigned char g_RTCAlarmFlag = FALSE, g_ExtiFlag = FALSE, g_ExtiRainfallFlag = FALSE, g_ExtiRainfallFlag2 = FALSE;

static unsigned char s_EnterStopFlag = FALSE, s_StopTime = 0;

unsigned char GetStopModeFlag(void)
{
	return s_EnterStopFlag;
}

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime)
{
	s_EnterStopFlag = isTrue;
	s_StopTime = StopTime;
}


void Clear_Flag(void)
{
	if(GetInputChannelCount() != 0)
	{
		g_485SensorGetFlag = FALSE;
	}
}



//进入停止模式前的配置，GPIO、中断、外设，使用快速唤醒模式

void StopModeProcess(void)
{
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	if(GetStopModeFlag() && g_PowerDetectFlag)
	{
		if(p_sys->Type == NETTYPE_LORA)
		{
			EnterStopMode(s_StopTime);
		}
		else
		{
			EnterStopMode_SIM(s_StopTime);
		}
	}
}

void EnterStopMode(unsigned char Second)	
{  	
	
	if(Second == 0)
	{
		return;
	}	
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 

	USART_Cmd(USART2,DISABLE); 
	USART_DeInit(USART2);
	USART_Cmd(USART3,DISABLE); 
	USART_DeInit(USART3);		
	__disable_irq();
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 
				
	RTC_SetAlarmA(Second);
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);

	DMA_Cmd(DMA1_Channel4, DISABLE ); 
	DMA_DeInit(DMA1_Channel4);
	
	SetLEDFlag(FALSE);
	Stop_GPIO_Init();	

//	EXTI5_Config();
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	

}

void EnterStopMode_SIM(unsigned char Second)	
{  	
	if(Second == 0)
	{
		return;
	}	
	
	Adc_Reset();
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 	
		
	__disable_irq();

	SIM_PWR_OFF();
	
//	u1_printf(" Stop\r\n");
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 	
	
	RTC_SetAlarmA(Second);
		
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);
	
	DMA_Cmd(DMA1_Channel4, DISABLE ); 
	DMA_DeInit(DMA1_Channel4);
	
	Stop_GPIO_Init_SIM();		
	
//	EXTI5_Config();
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	
}

void LowPower_Process(void)
{
	static unsigned int n_OverTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	

	if(g_RTCAlarmFlag)			//闹钟唤醒
	{
		g_RTCAlarmFlag = FALSE;
		
		Wake_SysInit();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
		
		u1_printf("\r\n %02d:%02d:%02d ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
		
		EXTI17_Reset();		
		
		if(DifferenceOfRTCTime(GetRTCSecond(), n_OverTime) >= 60)
		{
			n_OverTime = GetRTCSecond();
			
			g_PowerDetectFlag = FALSE;
		}
	}
	
	if(g_ExtiFlag)				//外部唤醒   LORA 唤醒模式使用
	{			
		g_ExtiFlag = FALSE;
		
		Wake_SysInit();
		
//		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
//		u1_printf("\r\n [%02d:%02d:%02d]  Aux\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
		
		EXTI17_Reset();
		
	}	
}
void RTC_Alarm_IRQHandler(void)	//闹钟中断
{
	if(RTC_GetFlagStatus(RTC_FLAG_ALRAF) != RESET)
	{		
		if(s_EnterStopFlag == TRUE)
		{
			s_EnterStopFlag = FALSE;
					
			SysClockForMSI(RCC_MSIRange_6);
						
			IWDG_ReloadCounter();
			
			if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
			{
				PWR_ClearFlag(PWR_FLAG_WU);//????
			}
		}
				
		EXTI_ClearITPendingBit(EXTI_Line17);
		
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		
		g_RTCAlarmFlag = TRUE;
	}	
}

void EXTI15_10_IRQHandler(void)	//外部中断	脉冲计数2
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	if(g_PluseChannel_1_Flag)
	{
		GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
		
		GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);	
		if(EXTI_GetITStatus(EXTI_RAINFALL_LINE) != RESET)
		{			
			if(s_EnterStopFlag == TRUE)
			{
				s_EnterStopFlag = FALSE;
				
				SysClockForMSI(RCC_MSIRange_6);
				
				if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
				{
					PWR_ClearFlag(PWR_FLAG_WU);//????
				}
				
				IWDG_ReloadCounter();
			}
			g_ExtiRainfallFlag = TRUE;
				
			EXTI_ClearITPendingBit(EXTI_RAINFALL_LINE);
		}
	}
	
	if(g_PluseChannel_2_Flag)
	{
		GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
		
		GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);	
		
		if(EXTI_GetITStatus(EXTI_RAINFALL2_LINE) != RESET)
		{			
			if(s_EnterStopFlag == TRUE)
			{
				s_EnterStopFlag = FALSE;
				
				SysClockForMSI(RCC_MSIRange_6);
				
				if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
				{
					PWR_ClearFlag(PWR_FLAG_WU);//????
				}
				
				IWDG_ReloadCounter();
			}
			g_ExtiRainfallFlag2 = TRUE;
				
			EXTI_ClearITPendingBit(EXTI_RAINFALL2_LINE);
		}
	}	
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

