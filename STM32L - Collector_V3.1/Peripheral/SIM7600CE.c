#include "main.h"
#include "SIM7600CE.h"

#define 	MCC_CHINA		460
#define 	MCC_VIETNAM		452

static u8 s_SIMConnectedFlag = FALSE;	//SIM连接成功标志



const char APN_TAB[11][20] = {
	"CMIOT",					//中国移动 "CMNET","CMIOT"
	"UNIM2M.NJM2MAPN",			//中国联通 "UNINET","UNIM2M.NJM2MAPN"
	"ctnet",					//中国电信
	"m-wap",					//越南MobiFone
	"m3-world",		//m-world 	//越南Vinaphone
	"NC",						//越南S-Fone				暂不支持
	"v-internet",				//越南Viettel
	"NC",						//越南Vietnamobile		暂不支持
	"NC",						//越南EVNTelecom			暂不支持
	"NC",						//越南Beeline			暂不支持
	"NC"						//越南3G EVNTelecom		暂不支持
};

void SIM7600CEPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);	 

	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM7600CE_ENABLE_TYPE, &GPIO_InitStructure);	


	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RST_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RST_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RF_DISABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RF_DISABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_DTR_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_DTR_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(SIM7600CE_DTR_TYPE, SIM7600CE_DTR_PIN);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_SetBits(GPIOB, GPIO_Pin_5);
	GPIO_SetBits(GPIOB, GPIO_Pin_6);	
	
	SIM7600CE_PWR_OFF();
	SIM7600CE_RST_ON();
	SIM7600CE_RF_DISABLE_ON();
}

void SIM7600CEProcess(unsigned short nMain10ms)
{
	u16 nTemp = 0;
	char str[100];
	static u16 s_LastTime = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_APNFlag = 0, s_APNTag = 0, s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0, s_Reboot = 0;
	static u8 s_PowerOnFlag = FALSE;
	static u8 s_ReportConfigFlag = TRUE;
	u16 MCC = 0;
	u8  MNC = 0;
	char * p1 = NULL, buf[10];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	static u8 s_LastRTCDate = 32;
	static u8 s_StartCalTimeFlag = TRUE;	//开始计算上报时间标志
	static u16 s_StartTime = 0, s_EndTime = 0;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
		s_LastRTCDate = RTC_DateStruct.RTC_Date;		
	}		
		
	switch(s_State)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 2)
			{
				s_RTCSumSecond = GetRTCSecond();	
				u1_printf(" OverTime\r\n");
				s_Reboot++;
				if(s_Reboot >= 6)
				{
					s_Reboot = 0;
					u1_printf(" Restart\r\n");
					delay_ms(100);
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();	
				}
				s_RetryCount = 0;
				s_State = 53;
				break;
			}
			SIM7600CEPortInit();

			SetTCProtocolRunFlag(FALSE);
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("\r\n Power Off 1s.\r\n");
			s_PowerOnFlag = TRUE;
		break;
		 
		case 2:	//断电后延时3S上电，模块上电自动开机				
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1600 )
			{
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 100 )
			{
				if(s_PowerOnFlag)
				{
					if(s_StartCalTimeFlag)
					{
						s_StartCalTimeFlag = FALSE;
						s_StartTime = GetSystem10msCount();
					}
					s_PowerOnFlag = FALSE;
					u1_printf("\r\n Power On 16s.\r\n");
					SIM7600CE_PWR_ON();	
				}
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
			{
				u1_printf("\r\n Check\r\n");
				u2_printf("AT\r");
				delay_ms(50);
				u2_printf("AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 4:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{
				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 20 )
			{				
					u1_printf("AT无响应\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 5 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_State = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

				switch(s_Step)
				{
					case 1:	u2_printf("ATE0\r");			break;
					case 2:	u2_printf("AT+CPIN?\r");		break;
					case 3:	u2_printf("AT+CIMI\r");			break;
					case 4:				//根据CIMI信息设置APN
						u2_printf("AT+CGDCONT=1,\"IP\",\"%s\"\r", (char *)APN_TAB[s_APNTag]);						
					break;
					case 5:	u2_printf("AT+CSQ\r");			break;	
					case 6:	u2_printf("AT+COPS?\r");		break;	
					case 7:	u2_printf("AT+CPSI?\r");		break;	
					case 8:	u2_printf("AT+CGATT?\r");		break;	
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}	
					
		break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(g_Uart2RxFlag == TRUE)
				{					
					u1_printf("%s\r\n", g_USART2_RX_BUF);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;					
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								u1_printf("\r\n SIM OK\r\n");
							}
							else
							{
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
								}
							}						
						break;	
							
						case 3:			//CIMI查询
							//中国移动:46000、46002、46004、46007、46008
							//中国联通:46001、46006、46009
							//中国电信:46003、46005、46011。
								//越南MCC:452
								//越南MNC   	运营商				APN:
								// 	  01 	MobiFone			m-wap
								// 	  02 	Vinaphone			m3-world / m-world
								// 	  03 	S-Fone 
								// 	  04 	Viettel Mobile		v-internet
								// 	  05 	Vietnamobile
								// 	  06 	EVNTelecom
								// 	  07 	Beeline VN
								// 	  08 	3G EVNTelecom
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )			//===============此处判断可能容易出错  
							{
								MCC = (g_USART2_RX_BUF[2] - '0')*100 + (g_USART2_RX_BUF[3] - '0')*10 +(g_USART2_RX_BUF[4] - '0');
								MNC = (g_USART2_RX_BUF[5] - '0')*10 + (g_USART2_RX_BUF[6] - '0');
								
								u1_printf("MCC=%d, MNC=%d\r\n",MCC,MNC);
								
								s_APNFlag = 0;	
								switch(MCC)
								{
									case	MCC_CHINA:
										u1_printf("MCC_CHINA\r\n");	
										switch(MNC)
										{
											//移动卡
											case 0:
											case 2:
											case 4:
											case 7:
											case 8:
												s_APNFlag = 1;	
												s_APNTag = CMNET;
												break;
											//联通卡
											case 1:
											case 6:
											case 9:
												s_APNFlag = 1;	
												s_APNTag = UNINET;
												break;	
											//电信卡
											case 3:
											case 5:
											case 11:
												s_APNFlag = 1;	
												s_APNTag = CTNET;
												break;										
											default:
												s_APNFlag = 0;	
												s_APNTag = MCC_UNKOWN;
												break;
										}
										break;
										
									case	MCC_VIETNAM:
										switch(MNC)
										{
											//移动卡
											case 1:
												s_APNFlag = 1;	
												s_APNTag = MOBIFONE;
												break;
											case 2:
												s_APNFlag = 1;	
												s_APNTag = VINAPHONE;
												break;
											case 4:
												s_APNFlag = 1;	
												s_APNTag = VIETTEL;
												break;							
											default:
												s_APNFlag = 0;	
												s_APNTag = MCC_UNKOWN;
												break;
										}
										break;
									default:
										s_APNFlag = 0;	
										s_APNTag = MCC_UNKOWN;
										break;
								}						

								if(s_APNFlag)
								{
									u1_printf("IMSI: APN SET TO %s\r\n", (char *)APN_TAB[s_APNTag]);
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;		
								}
								else
								{
									u1_printf("IMSI: GET IMSI FAIL\r\n");
									s_State--;		
									s_ErrCount ++;	
								}
									
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 4:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
							//	u1_printf("APN OK\r\n");
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
														
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 5:				//	u1_printf("检测信号质量： ");					
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", g_USART2_RX_BUF);							
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								if( nTemp == 99 )								
								{
									s_State--;	
									s_ErrCount++;
									u1_printf(" 无信号\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else if( nTemp < 10 )								
								{
									s_State--;	
									s_ErrCount++;
									u1_printf(" 信号差\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else
								{
									s_State--;	
									s_Step++;	
									s_ErrCount = 0;
									u1_printf("\r\n  信号正常\r\n");
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);									
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}		
						break;		
							
						case 6:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
							{		
								if((strstr((const char *)g_USART2_RX_BUF,"CHINA MOBILE")) != NULL )  
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"CHINA TELECOM")) != NULL )  	//电信运营商信息不一定正确
								{									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"Vinaphone")) != NULL )  	//越南vinaphone
								{									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if(s_APNFlag)  	//电信运营商信息不一定正确
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else
								{
									s_State--;		
									s_ErrCount ++;
								}
									
							//	u1_printf("%s\r\n", g_USART2_RX_BUF);
							}
							else
							{
								u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}
						break;	
											
						case 7:
							//u1_printf(" 模块注册网络： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CPSI:")) != NULL )
							{						
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
								{
									s_State--;
									s_Step++;
									s_ErrCount = 0;
								}
								else
								{
									s_State--;
									s_ErrCount++;
									u1_printf("\r\n Fail,Count=%d\r\n",s_ErrCount);
								}	
								
								if(s_ErrCount >= 20)
								{		
									u1_printf("\r\n  Restart\r\n");
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
								}
								
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}	
								
						break;						
													
						case 8:
							u1_printf("GPRS附着状态： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
							{					
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								
								nTemp = atoi( (const char *) (p1+8));
								if( nTemp == 0 )								
								{
									u1_printf(" 未附着\r\n");
									s_State--;	
									s_ErrCount++;
									if(s_ErrCount >= 30)
									{					
//										u1_printf("\r\n  网络附着失败，重启模块\r\n");
										s_ErrCount = 0;
										s_State = 1;
										s_Step = 1;
									}
								}
								else
								{
//									u1_printf(" 附着成功\r\n");
//									if(GetGPSRunFlag() && GetGPSDataFlag() == FALSE)	// && ((s_APN = APN_CMNET) || (s_APN = APN_UNINET)) )	//移动和联通卡可进行定位
//									{
//										u1_printf("LBS定位\r\n");
//										s_State++;	
//									}
//									else
//									{
//										u1_printf("不定位\r\n");;
//										s_State = 9;	
//									}
									s_State = 9;
									s_Step = 1;	
									s_ATAckCount = 0;
									s_ErrCount = 0;
								}
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}							
						break;
					}
					
					if(s_ErrCount > 50 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					Clear_Uart2Buff();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
				{
					s_ATAckCount++;
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						s_ErrCount = 0;
						u1_printf(" AT No Ack\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_State = 1;
						s_Step = 1;
					}
					else
					{
						s_State--;
						u1_printf(" AT指令超时\r\n");
					}
										
				}
				s_LastTime = GetSystem10msCount();
				
			}
				
		
		break;

//		case 7:			//电信LBS定位指令不同
//			if((s_APNTag == CMNET) || (s_APNTag == UNINET))		//移动联通卡LBS定位
//			{
//				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
//				{
//					switch(s_Step)
//					{
//						case 1:	u2_printf("AT+CNETSTART\r");		break;
//						case 2:	u2_printf("AT+CNETIPADDR?\r");		break;
//						case 3:	u2_printf("AT+CLBS=1\r");			break;
//						case 4:	u2_printf("AT+CNETSTOP\r");			break;
//						default:
//							s_Step = 1;	
//							s_State = 9;			//跳过LBS定位
//							s_ATAckCount = 0;
//							s_ErrCount = 0;
//						break;
//					}
//					s_State++;
//					s_LastTime = GetSystem10msCount();	
//				}
//			}
//			else 		//电信卡定位：	
//			{
//				s_State = 9;	//跳过LBS定位
//				s_LastTime = GetSystem10msCount();	
//			}
//		break;
//			
//		case 8:
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10)
//			{
//				if(g_Uart2RxFlag == TRUE)
//				{
//					u1_printf("%s\r\n", g_USART2_RX_BUF);
//												
//					switch(s_Step)
//					{	
//						case 1:						
//							if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTART: 0")) != NULL )
//							{									
//								s_Step++;			
//								s_State--;	
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}				
//							else if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTART: 1")) != NULL )
//							{					
//								u1_printf("正在启动。。\r\n");						
//								s_State--;	
//								s_ErrCount ++;
//							}	
//							else	
//							{
////								u1_printf("收到未识别的指令\r\n");	
//								s_State--;	
//								s_ErrCount ++;
//							}
//						break;					
//							
//						case 2:
//							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
//							{					
//								s_Step ++;	
//								s_State--;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}
//							else
//							{
//								u1_printf("    Error\r\n");
//								s_State--;	
//								s_ErrCount++;
//							}						
//						break;	
//							
//						case 3:
//							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
//							{
//								s_Step++;	
//								s_State--;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//								memcpy(str, g_USART2_RX_BUF, g_USART2_RX_CNT);
//								p_str = strtok(str, ",");
//								p_str = strtok(NULL, ",");  
//								SetLatitude(atof(p_str));
//								p_str = strtok(NULL, ",");  
//								SetLongitude(atof(p_str));
//								s_LocationFailCount = 0;
//								u1_printf("\r\n %.5f,%.5f \r\n", GetLongitude(), GetLatitude());
//								SetGPSRunFlag(FALSE);
//							}
//							else
//							{
//								s_State--;	
//								s_ErrCount++;								
//							}
//						break;
//							
//						case 4:
//							if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTOP: 0")) != NULL )
//							{					
//								s_Step = 1;	
//								s_State++;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}
//							else
//							{
//								s_State--;	
//								s_ErrCount++;
//							}						
//						break;
//							
//						default:

//						break;
//							
//					}		
//					if(s_ErrCount > 5 )
//					{
////						u1_printf(" LBS定位失败, 关闭LBS\r\n");
//						u2_printf("AT+CNETSTOP\r");
//						SetLongitude(0);
//						SetLatitude(0);
//						s_LocationFailCount++;
//						if(s_LocationFailCount >= 3)
//						{
//							s_LocationFailCount = 0;
//							SetGPSRunFlag(FALSE);
//						}
//						s_ErrCount = 0;
//						s_ATAckCount = 0;
//						s_State = 9;
//						s_Step = 1;
//					}
//					
//					Clear_Uart2Buff();
//				}
//				else
//				{		
//					s_ATAckCount++;			
//					if(s_ATAckCount >= 5 )
//					{
////						u1_printf(" LBS定位超时, 关闭LBS\r\n");
//						u2_printf("AT+CNETSTOP\r");
//						SetLongitude(0);
//						SetLatitude(0);
//						s_LocationFailCount++;
//						if(s_LocationFailCount >= 3)
//						{
//							s_LocationFailCount = 0;
//							SetGPSRunFlag(FALSE);
//						}
//						s_ErrCount = 0;
//						s_ATAckCount = 0;
//						s_State ++;
//						s_Step = 1;
//					}
//					else
//					{				
//						u1_printf(" 响应超时\r\n");		
//						s_State--;	
//					}
//										
//				}				
//				s_LastTime = GetSystem10msCount();		
//			}
//		break;		
			
		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				switch(s_Step)
				{
						case 1:	u2_printf("AT+CIPMODE=1\r");			break;
						case 2:	u2_printf("AT+NETOPEN\r");				break;
						case 3:	u2_printf("AT+IPADDR\r");				break;						
						default:
							s_State = 1;
							s_Step = 1;
						break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		
		break;
	
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(g_Uart2RxFlag == TRUE)
				{
					u1_printf("%s\r\n", g_USART2_RX_BUF);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
							//	u1_printf("设置为透传模式\r\n");
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
													
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
							if((strstr((const char *)g_USART2_RX_BUF,"+NETOPEN: 0")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
														
							}
							else if((strstr((const char *)g_USART2_RX_BUF,"already opened")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 3:
							//u1_printf("模块本地IP：");
							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
							{					
								s_Step = 1;	
								s_State++;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
							
								//sprintf(str, "%s\r\n", g_USART2_RX_BUF));
								//u1_printf(str);
							}
							else
							{
								u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}						
						break;
						
						default:
							
						break;
							
					}
					
					if(s_ErrCount > 50 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_Uart2Buff();
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);	
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT No Ack\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 11:
			u1_printf("建立TCP\r\n");
			sprintf(str,"AT+CIPOPEN=0,\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			u2_printf(str);	
			u1_printf(str);	//串口输出AT命令
			u1_printf("\n");
			s_LastTime = GetSystem10msCount();
			s_State++;		
		//	s_ErrCount = 0;		
		break;
		
		case 12:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(g_Uart2RxFlag == TRUE)
				{
					u1_printf("%s\r\n", g_USART2_RX_BUF);
													
					if((p1 = (strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL"))) != NULL )
					{					
						s_ErrCount++;
						s_State--;
					}							
					else if((p1 = (strstr((const char *)g_USART2_RX_BUF,"CONNECT 115200"))) != NULL )
					{	
						s_Step = 1;	
						s_State++;		
						s_ErrCount = 0;
						SetTCModuleReadyFlag(TRUE);		//连接成功========================					
					}								
					else if((p1 = (strstr((const char *)g_USART2_RX_BUF,"+CIPOPEN:"))) != NULL )
					{
						nTemp = atoi( (const char *) (p1+12));
						if(nTemp == 0)
						{						
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);			//连接成功========================			
						}
						else
						{
							s_ErrCount++;
							s_State--;
						}						
					}
					else
					{
						s_State--;	
						s_ErrCount++;
					}
						
					
					if(s_ErrCount > 5 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_Uart2Buff();

				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						u1_printf(" AT No Ack\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_State = 1;
						s_Step = 1;
						s_LastTime = GetSystem10msCount();
						s_ErrCount++;
					}
					else
					{
						s_State--;
						u1_printf(" AT Timeout\r\n");
					}
				}	
			}				
		break;
		
		case 13:
			s_OverCount = 0;
			s_State = 50;
			SetServerConfigCmdFlag(FALSE);
			s_LastTime = GetSystem10msCount();
			SetTCProtocolRunFlag(TRUE);
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(g_PowerDetectFlag && g_485SensorGetFlag)
				{
					if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	//发送心跳包
					{
						s_AliveRTCCount = GetRTCSecond();
						Mod_Send_Alive(SIM_COM);
						delay_ms(200);
						break;
					}	
					
//					if(s_ReportConfigFlag)	//主动上报配置信息,只上报一次，无应答
//					{
//						s_ReportConfigFlag = FALSE;
//						ReportSystemConfig();
//						delay_ms(200);						
//						break;
//					}
					
					if(g_TCRecStatus.DataAckFlag == FALSE)
					{
						s_EndTime = GetSystem10msCount();
						SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
						Mod_Report_Data(SIM_COM);
					}
					
					s_State++;
					s_LastTime = GetSystem10msCount();
				}

			}
			else
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(g_TCRecStatus.DataAckFlag)//非长连接设备取消心跳包  2019.2.12
			{
				p_sys = GetSystemConfig();
				SetLogErrCode(LOG_CODE_SUCCESS);
				if(p_sys->LowpowerFlag == 0)
				{
					s_State = 52;	//低功耗设备
					WriteCommunicationSuccessCount();
					SetTCProtocolRunFlag(FALSE);
					s_SIMConnectedFlag = TRUE;
				}
				else if(p_sys->LowpowerFlag == 1)
				{
					s_State = 56;	//非低功耗设备
				}
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.DataAckFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				Clear_Uart2Buff();
				s_OverCount++;
				
				if(p_sys->LowpowerFlag == 0)
				{
					if(s_OverCount >= 2)
					{
						SetLogErrCode(LOG_CODE_NOSERVERACK);
						s_OverCount = 0;
						s_State = 53;
						s_Step = 1;
						s_ErrCount = 0;
						u1_printf(" Link Fail, Sleep\r\n");
					}
				}
			}		
		break;
					
		case 52:
			if(GetCmdCloseSocket() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n Wait server over time, sleep\r\n");
					s_State++;
				}
			}
			else if(GetCmdCloseSocket())		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n Rec server ack,sleep\r\n");
				s_State++;
				SetCmdCloseSocket(FALSE);
			}
		break;
			
		case 53:
			s_State = 54;
			s_Step = 1;
			s_RTCSumSecond = GetRTCSecond();
			
			u1_printf("\r\n Link Time:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
			s_LastTime = GetSystem10msCount();	
			StoreOperationalData();	
			WriteCommunicationCount();
		break;
			
		case 54:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				SetLogErrCode(LOG_CODE_CLEAR);	
				SetTCProtocolRunFlag(FALSE);	
				u1_printf("\r\n [%02d:%02d:%02d]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
				s_ReportConfigFlag = TRUE;
				s_StartCalTimeFlag = TRUE;
				s_SIMConnectedFlag = FALSE;
				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State++;
			}		
			else			//未到上报时间，继续休眠
			{
					
				if(s_SIMConnectedFlag)
				{
					SetStopModeTime(TRUE, STOP_TIME);	
				}
				else
				{
					SetStopModeTime(TRUE, ERR_STOP_TIME);	
				}
				
			}
		break;
			
		case  55:

			s_State = 1;
			s_Step = 1;
			s_ErrCount = 0;
			s_LastTime = GetSystem10msCount();
			SetTCModuleReadyFlag(FALSE);
		
			RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
			
			if(s_LastRTCDate != RTC_DateStruct.RTC_Date)	//日期更新，获取经纬度
			{
				s_LastRTCDate = RTC_DateStruct.RTC_Date;
				SetGPSRunFlag(TRUE);
				SetGPSPowerFlag(TRUE);
			}

		break;		
			
		case 56:
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = GetSystem10msCount();

		break;
		
		case 57:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State = 50;
			}	
			if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	//发送心跳包
			{
				s_AliveRTCCount = GetRTCSecond();
				Mod_Send_Alive(SIM_COM);
			}			
		break;
			
		default:
			s_State = 1;
			u1_printf(" 意外错误!\r\n");
		break;
	}
	
}
