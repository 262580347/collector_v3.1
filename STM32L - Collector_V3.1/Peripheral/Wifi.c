#include "Wifi.h"
#include "main.h"

#define COM_DATA_SIZE	256

typedef __packed struct
{
	unsigned char 			magic; 	//0x55
	unsigned short 			length;	//存储内容长度	
	unsigned char 			chksum;	//校验和
	unsigned char 			data[100];		//设备ID
}WIFIPASSWORD;

static void WiFiObser(unsigned short nMain10ms);

static u8 m_CommBuff[COM_DATA_SIZE];

static u8 s_WiFiData[100] ;

static u16 m_CirWPinter = 0;

static u16 m_CirRPinter = 0;

static u8 s_HeartPackFlag = FALSE;

static u8 s_DataPackFlag = FALSE;

static u8 s_WiFiReadyFlag = FALSE;

static u8 s_WiFiResetFlag = FALSE;

static void Clear_COMBuff(void)
{
	u16 i;
	
	for(i=0; i<COM_DATA_SIZE; i++)
	{
		m_CommBuff[i] = 0;		
	}
		m_CirWPinter = 0;

		m_CirRPinter = 0;	
}

void OnWiFiComm(u8 CommData)
{
	m_CommBuff[m_CirWPinter++] = CommData;
	
	if(m_CirWPinter >= COM_DATA_SIZE)
	{
		m_CirWPinter = 0;
	}
	if(m_CirWPinter == m_CirRPinter)
	{
		m_CirRPinter++;
		if(m_CirRPinter	>= COM_DATA_SIZE)
		{
		    m_CirRPinter = 0; 
		}
	}	
}

unsigned char Set_WiFi_AP(unsigned char *WiFiPassword, unsigned char lenth)
{
	u8  i;
	WIFIPASSWORD WiFiConfig;
	
	FLASH_ErasePage(WIFI_AP_ADDR);
	WiFiConfig.magic = 0x55;
	WiFiConfig.length = lenth;
	WiFiConfig.chksum = Calc_Checksum((unsigned char *)WiFiPassword, lenth);
	
	memcpy((void *)&WiFiConfig.data, WiFiPassword, lenth);
	WriteFlash(WIFI_AP_ADDR , (unsigned char *)&WiFiConfig, lenth+4);
	
	
	if (Check_Area_Valid(WIFI_AP_ADDR))
	{
		for(i=0; i<lenth; i++)
		{
			s_WiFiData[i] = ReadFlash(WIFI_AP_ADDR+i+4);
		}
		printf("\r\n 设置新WIFI数据成功\r\n");
	}
	else
	{
		printf("读取失败\r\n");
		for(i=0; i<lenth+4; i++)
		{
			s_WiFiData[i] = ReadFlash(WIFI_AP_ADDR+i);
		}
		for(i=0; i<lenth+4; i++)
			printf("%c", s_WiFiData[i]);
		printf("\r\n");
		for(i=0; i<lenth+4; i++)
			printf("%02X", s_WiFiData[i]);
		printf("\r\n");
	}
	
	return 0;
}

void WiFi_Port_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = WIFI_ENABLE_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(WIFI_ENABLE_TYPE, &GPIO_InitStructure);	
}

unsigned char WiFiInit(void)
{
	static u8 s_State = 1, s_Step = 1, s_ErrorCount = 0, s_WiFiErrorFlag = FALSE;
	static u16 s_LastTime = 0, s_LastErrTime = 0;
	static u32 s_RTCTime = 0;
	u16	nMain10ms, i, lenth;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();	
	nMain10ms = GetSystem10msCount();
	
	if(s_ErrorCount >= 3)
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) >= p_sys->Heart_interval)
		{
			s_ErrorCount = 0;
			s_State = 0;
			s_Step = 1;
			s_WiFiResetFlag = FALSE;
		}
		else
		{
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
			printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
			EnterStopMode_WIFI(24);	
			return TRUE;
		}
	}
	
	switch(s_State)
	{
		case 0:
			Uart_Send_Str(WIFI_COM, "AT+RST\r\n");
			delay_ms(300);
			Clear_Uart3Buff();
			USART3_Config(115200);
			s_State++;
		break;
		
		case 1:
			if(s_WiFiErrorFlag == TRUE)
			{
				if(CalculateTime(nMain10ms, s_LastErrTime) >= 1000) 
				{
					s_WiFiErrorFlag = FALSE;
					s_LastErrTime = nMain10ms;	
					s_LastTime = nMain10ms;
					s_Step = 5;
				}
				break;
			}
			if(CalculateTime(nMain10ms, s_LastTime) >= 10) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(s_Step)
				{
					case 1:
						Uart_Send_Str(WIFI_COM, "AT\r\n");
						printf("[COM2]->AT\r\n");
						s_LastTime = nMain10ms;
					break;
					case 2:
						Uart_Send_Str(WIFI_COM, "ATE0\r\n");
						printf("[COM2]->ATE0\r\n");
						s_LastTime = nMain10ms;
					break;
					case 3:
						Uart_Send_Str(WIFI_COM, "AT+CWMODE=1\r\n");
						printf("[COM2]->AT+CWMODE=1\r\n");
						s_LastTime = nMain10ms;
					break;
					case 4:
						Uart_Send_Str(WIFI_COM, "AT+CWAUTOCONN=0\r\n");
						printf("[COM2]->AT+CWAUTOCONN=0\r\n");
						s_LastTime = nMain10ms;
					break;
					case 5:
						if(Check_Area_Valid(WIFI_AP_ADDR))
						{
							lenth = ReadFlash(WIFI_AP_ADDR+1) + (ReadFlash(WIFI_AP_ADDR+2) << 8);
							for(i=0; i<lenth; i++)
							{
								s_WiFiData[i] = ReadFlash(WIFI_AP_ADDR+i+4);
							}
							Uart_Send_Str(WIFI_COM, "AT+CWJAP=");
							Uart_Send_Str(WIFI_COM, (char *)s_WiFiData);
							Uart_Send_Str(WIFI_COM, "\r\n");
							printf("[COM2]->AT+CWJAP=");
							for(i=0; i<lenth; i++)
								printf("%c", s_WiFiData[i]);
							printf("\r\n");
						}
						else
						{
							Uart_Send_Str(WIFI_COM, "AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
							printf("[COM2]->AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");		
						}
						s_LastTime = nMain10ms;
					break;
					case 6:
						Uart_Send_Str(WIFI_COM, "AT+CIPMUX=0\r\n");
						printf("[COM2]->AT+CIPMUX=0\r\n");
						s_LastTime = nMain10ms;
					break;
					case 7:
						Uart_Send_Str(WIFI_COM, "AT+CIPMODE=1\r\n");
						printf("[COM2]->AT+CIPMODE=1\r\n");
						s_LastTime = nMain10ms;
					break;
					case 8:
						Uart_Send_Str(WIFI_COM, "AT+CIPSTART=\"TCP\",\"42.159.233.88\",6070\r\n");
						printf("[COM2]->AT+CIPSTART=\"TCP\",\"42.159.233.88\",6070\r\n");
						s_LastTime = nMain10ms;
					break;
					case 9:
						Uart_Send_Str(WIFI_COM, "AT+CIPSEND\r\n");
						printf("[COM2]->AT+CIPSEND\r\n");
						s_LastTime = nMain10ms;
					break;		
					
					case 20:
						Uart_Send_Str(WIFI_COM, "AT+CWQAP\r\n");
						printf("[COM2]->AT+CWQAP\r\n");
						s_LastTime = nMain10ms;
					break;
				}
				s_State++;
			}
		break;
		
		case 2:
			if(g_Uart3RxFlag == TRUE)
			{
				g_Uart3RxFlag = FALSE;
				
				for(i=0; i<g_USART3_RX_CNT; i++)
					printf("%c", g_USART3_RX_BUF[i]);
				printf("\r\n");
											
				switch(s_Step)
				{
					case 1:
					case 2:
					case 3:
					case 4:
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_LastTime = nMain10ms;							
						}
					break;
					
					case 5:
						if((strstr((const char *)g_USART3_RX_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_LastTime = nMain10ms;
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI GOT IP")) != NULL )
						{
							s_LastTime = nMain10ms;
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							s_Step++;
							s_State--;	
							s_LastTime = nMain10ms;
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI DISCONNECT")) != NULL )
						{
							printf("\r\n WIFI链接错误\r\n");
							s_LastTime = nMain10ms;
//							s_Step = 20;
//							s_State--;	
//							s_LastErrTime = nMain10ms;
//							s_LastTime = nMain10ms;
						}
						
					break;
						
					case 6:
					case 7:	
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_LastTime = nMain10ms;							
						}
					break;
						
					case 8:
						if((strstr((const char *)g_USART3_RX_BUF,"CONNECT")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_LastTime = nMain10ms;							
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )
						{
							s_State--;		
							s_LastTime = nMain10ms;
							delay_ms(500);
						}
							
					break;
						
					case 9:
						if((strstr((const char *)g_USART3_RX_BUF,">")) != NULL )
						{
							s_Step = 1;	
							s_State++;		
							s_LastTime = nMain10ms;							
						}
					break;	
						
					case 20:
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							s_WiFiErrorFlag = TRUE;
							s_State--;	
							s_LastTime = nMain10ms;
						}
					break;
						
				}
				
				Clear_Uart3Buff();

			}
			else if(CalculateTime(nMain10ms, s_LastTime) >= 1000) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_State = 0;
				s_Step = 1;
				printf(" AT无响应\r\n");
				s_LastTime = nMain10ms;
				s_ErrorCount++;
				s_RTCTime = GetRTCSecond();
			}
		break;
		
		case 3:
			printf("\r\n 成功建立TCP链接\r\n");
			s_WiFiReadyFlag = TRUE;
			s_State++;
			
		break;
		
		case 4:
			if(s_WiFiResetFlag)
			{
				printf(" \r\n WIFI重连\r\n");

				s_ErrorCount = 0;
				s_State = 1;
				s_Step = 1;
				s_LastTime = nMain10ms;
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		break;
		
		default:
			
		break;
		
	}
	
	return FALSE;
}

void WiFiProcess(unsigned short nMain10ms)
{
	static u16  s_LastTime = 0, s_OverCount = 0;
	static unsigned int s_RTCSumSecond = 0;
	static u8 s_State = 1,RunOnce = FALSE;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE || s_WiFiResetFlag)
	{	
		RunOnce = TRUE;
			
		WiFi_Port_Init();
		
		WIFI_PWR_OFF;
		printf("\r\n WIFI_PWR_OFF\r\n");
		delay_ms(50);	
		
		WIFI_PWR_ON;
		printf("\r\n WIFI_PWR_ON\r\n");
		delay_ms(500);
		USART3_Config(115200);		
		Clear_Uart3Buff();		
		printf("\r\n 初始化 ESP8266\r\n");
		
		while(WiFiInit() == FALSE)
		{
			IWDG_ReloadCounter();
			ComProcess();
		}
		
		USART3_Config(115200);
		
		USART_ITConfig(USART3, USART_IT_IDLE, DISABLE);
		
		s_LastTime = nMain10ms;

		return;
	}
		
	WiFiObser(nMain10ms);

	switch(s_State)
	{
		case 1:
			if(s_WiFiReadyFlag)
			{
				if(g_PowerDetectFlag)
				{
					Mod_Send_Alive(WIFI_COM);
					s_State++;
					s_LastTime = nMain10ms;
				}
			}			
		break;
		
		case 2:
			if(s_HeartPackFlag && (CalculateTime(nMain10ms,s_LastTime) >= 2))
			{
				Mod_Report_Data(WIFI_COM);
				s_State++;
				s_LastTime = nMain10ms;
			}
			else
			{
				if( CalculateTime(nMain10ms,s_LastTime) >= 800 )
				{
					s_State--;
					s_LastTime = nMain10ms;
					printf("\r\n 超时未收到心跳回复包  重发\r\n");
					s_OverCount++;
					if(s_OverCount >= 3)
					{
						s_OverCount = 0;
						s_WiFiResetFlag = TRUE;
						s_WiFiReadyFlag = FALSE;
					}
				}
			}
		break;
		
		case 3:
			if(s_DataPackFlag)
			{
				s_State++;
				s_LastTime = nMain10ms;
				s_DataPackFlag = FALSE;
				s_HeartPackFlag = FALSE;
			}
			else
			{
				if( CalculateTime(nMain10ms,s_LastTime) >= 800 )
				{
					s_State--;
					s_LastTime = nMain10ms;
					printf("\r\n超时未收到数据回复包  重发\r\n");
					s_OverCount++;
					if(s_OverCount >= 5)
					{
						s_OverCount = 0;
						s_WiFiResetFlag = TRUE;
						s_WiFiReadyFlag = FALSE;
					}
				}
			}			
		break;
			
		case 4:
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = nMain10ms;
		break;
		
		case 5:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Heart_interval)
			{
				s_RTCSumSecond = GetRTCSecond();
				
				printf(" \r\n 重新注册\r\n");
				Clear_Flag();
				s_State++;
			}		
			else
			{
					s_LastTime = nMain10ms;
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

					EnterStopMode_WIFI(24);		
			}
		break;
			
		case  6:
			if(g_PowerDetectFlag)
			{
				s_State = 1;
				s_WiFiResetFlag = TRUE;
				s_WiFiReadyFlag = FALSE;
			}
		break;			
	}

}

void OnRecWiFiData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 i;
//	CLOUD_HDR *hdr;
//	static u8 s_RegistPackCount = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	RTC_DateTypeDef RTC_DateStruct;
	
//	SYSTEMCONFIG *p_sys;

//	p_sys = GetSystemConfig();
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	printf("\r\n [%02d:%02d:%02d][WIFI] <- 协议号：%02d 设备号：%04d 方向：%X 包序号：%04X 包长度：%X 命令字：%02X 数据:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
	for(i=0; i<lenth; i++)
		printf("%02X ", data[i]);
	printf("\r\n");

	if(pMsg->cmd == CMD_HEATBEAT)
	{
		printf("收到服务器心跳包回复,同步服务器时间\r\n");
			
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
				
		RTC_TimeStructure.RTC_Hours   = data[2];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[0];

		RTC_DateStruct.RTC_Date  	= data[3];
		RTC_DateStruct.RTC_Month  	= data[4];
		RTC_DateStruct.RTC_Year  	= data[5];	
		RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
		
		if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
		{
			printf("\r\n 同步远程时间成功\r\n");
			RTC_TimeShow();
		}
		else
		{
			printf("\r\n 同步远程时间失败\r\n");
		}
		
		s_HeartPackFlag = TRUE;
	}
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		printf("收到服务器上报数据回复,上报数据成功\r\n");
		
		s_DataPackFlag = TRUE;
	}
}
	
static void WiFiObser(unsigned short nMain10ms)
{
	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
	static CLOUD_HDR m_RxFrame;
	static u8 RxData, databuff[100];
	u8 checksum;
	
	if(s_HeadFlag == TRUE)
	{
		if(CalculateTime(nMain10ms, s_LastTime) >= 100)
		{
			s_HeadFlag = FALSE;
			printf("\r\n 接收超时\r\n");
			Clear_Uart3Buff();
			memset(databuff, 0, 100);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
	}
		
	if(m_CirRPinter != m_CirWPinter)
	{
		RxData = m_CommBuff[m_CirRPinter++];
		if(m_CirRPinter >= COM_DATA_SIZE)
		{
			m_CirRPinter = 0;
		}
		if(RxData == 0x7d && special_flag == FALSE)
		{
			special_flag = TRUE;
			return;
		}
		if(special_flag == TRUE)
		{
			special_flag = FALSE;
			if(RxData == 0x5d)
			{
				RxData = 0x7d;
			}
			else if(RxData == 0x5e)
			{
				RxData = 0x7e;
			}
			else if(RxData == 0x51)
			{
				RxData = 0x21;
			}
		}



		switch(FrameID)
		{
			case 0:
				if(RxData == 0X7e)
				{
					s_HeadFlag = TRUE;
					s_LastTime = nMain10ms;
					FrameID++;
				}
				break;
			case 1:
				m_RxFrame.protocol = RxData;
				FrameID++;
			break;
			case 2:
				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
				FrameID++;
			break;
			case 3:
				m_RxFrame.device_id = RxData;
				FrameID++;
			break;
			case 4:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 5:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 6:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			
			break;
			case 7:
				m_RxFrame.dir = RxData;
				FrameID++;
			break;
			case 8:
				m_RxFrame.seq_no = RxData;
				FrameID++;
			break;
			case 9:
				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
				FrameID++;
			break;
			case 10:
				m_RxFrame.payload_len = RxData;
				FrameID++;
			break;
			case 11:
				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
				data_lenth = m_RxFrame.payload_len;
				FrameID++;
			break;
			case 12:
				m_RxFrame.cmd = RxData;				
				i = 0;
			    memset(databuff, 0, 100);
				FrameID++;
				if(data_lenth == 0)
				{
					FrameID = 14;
					break;
				}	
				else if(data_lenth >= 100)
				{
					printf("\r\n too long %d\r\n", data_lenth );
					Clear_Uart3Buff();
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
			break;
			case 13:				
				databuff[i++] = RxData;
				if(i >= data_lenth)
				{
					FrameID++;
				}
			break;		
			case 14:
				checksum = RxData;
				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
				{
					printf("[SIM]Check error...");
					printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
					FrameID = 0;	
					s_HeadFlag = FALSE;					
					Clear_Uart3Buff();	
					Clear_COMBuff();
					s_HeadFlag = FALSE;		
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
				}else
				FrameID++;
			break;
			case 15:
				if(RxData == 0x21)	
				{								
					OnRecWiFiData(&m_RxFrame, databuff, data_lenth);
				}
				else
				{				
					printf("end error!\r\n");
				}
				s_HeadFlag = FALSE;		
				Clear_Uart3Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;				
			break;
			default:
				s_HeadFlag = FALSE;		
				Clear_Uart3Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;	 
			break;	
		}
	}	
}

