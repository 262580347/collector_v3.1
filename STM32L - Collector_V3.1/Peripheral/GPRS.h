#ifndef  _GPRS_H_
#define	 _GPRS_H_

#define		GPRS_PWR_PIN		GPIO_Pin_8
#define		GPRS_PWR_TYPE		GPIOA
#define		GPRS_PWR_ON()			GPIO_SetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);
#define		GPRS_PWR_OFF()		GPIO_ResetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);

void GPRS_Process(unsigned short nMain10ms);

void OnGPRSComm(unsigned char CommData);

unsigned char GetGPRSReadyFlag( void );




#endif
