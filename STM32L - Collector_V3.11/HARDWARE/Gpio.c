#include "main.h"
#include "Gpio.h"



void STM32_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 |  GPIO_Pin_7 |  GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_12  | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 |  GPIO_Pin_5 |  GPIO_Pin_6 |  GPIO_Pin_7 |  GPIO_Pin_8 | GPIO_Pin_9| GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);	
	
	if(GetBQ24650ENFlag() || GetForcedChargeFlag())
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_ENABLE();
	}
	else
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_DISABLE();
	}
	
	GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);			

	GPIO_InitStructure.GPIO_Pin = LED_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(LED_POWER_TYPE, &GPIO_InitStructure);		
	
	GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);

	GPIO_InitStructure.GPIO_Pin = RUN_MODE_PIN ;		//模式选择引脚，上拉为低功耗模式，下拉为工作模式
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_MODE_TYPE, &GPIO_InitStructure);	
}

void Stop_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 |  GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 |  GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	LORA_WORK_MODE();	
	
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);	
	
	if(GetBQ24650ENFlag() || GetForcedChargeFlag())
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_ENABLE();
	}
	else
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_DISABLE();
	}
	
	if(GetLoraState() == FALSE)
	{
		GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
		
		GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);			
	}
	
	GPIO_InitStructure.GPIO_Pin = RUN_MODE_PIN ;		//模式选择引脚，上拉为低功耗模式，下拉为工作模式
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_MODE_TYPE, &GPIO_InitStructure);	
}

void Stop_GPIO_Init_SIM(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 |  GPIO_Pin_7 |  GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12  | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 |  GPIO_Pin_5 |  GPIO_Pin_6 |  GPIO_Pin_7 |  GPIO_Pin_8 | GPIO_Pin_9| GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
			
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);
	
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_InitStructure.GPIO_Pin = EXTI_RAINFALL_GPIO_PIN;
//	GPIO_Init(EXTI_RAINFALL_GPIO_TYPE, &GPIO_InitStructure);
//		
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_InitStructure.GPIO_Pin = EXTI_RAINFALL2_GPIO_PIN;
//	GPIO_Init(EXTI_RAINFALL2_GPIO_TYPE, &GPIO_InitStructure);
			
	if(GetBQ24650ENFlag() || GetForcedChargeFlag())
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
			
		BQ24650_ENABLE();

	}
	else
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_DISABLE();
	}
	
}
void WakeUp_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);

	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 |  GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12  | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);		
				
	GPIO_InitStructure.GPIO_Pin = BAT_FULL_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_FULL_TYPE, &GPIO_InitStructure);	

	if(GetBQ24650ENFlag() || GetForcedChargeFlag())
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_ENABLE();
	}
	else
	{
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_DISABLE();
	}
	
	GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);	
	
	if(Get_Battery_Vol() <= 7700)
	{
		GPIO_InitStructure.GPIO_Pin = LED_POWER_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_POWER_TYPE, &GPIO_InitStructure);		
		
		GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);
	}
	
	GPIO_InitStructure.GPIO_Pin = RUN_MODE_PIN ;		//模式选择引脚，上拉为低功耗模式，下拉为工作模式
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_MODE_TYPE, &GPIO_InitStructure);	
	
}
void RunLED(unsigned short nMain10ms)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	static u8 RunOnce = FALSE;
	static u16 s_LastTime = 0, s_LastTime2 = 0, BlinkTime = 30;
	unsigned short BatVol = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	
	if(RunOnce == FALSE)
	{
		GPIO_InitStructure.GPIO_Pin = LED_POWER_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_POWER_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);

		GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		
		RunOnce = TRUE;
	}
	


	BatVol = Get_Battery_Vol();
	if(BatVol > 7700)			//电压值正常，不亮
	{
		GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);	
	}
	else if(BatVol <= 7700 && BatVol > 7000)			//电压低，电压越低闪烁越快
	{
		if(CalculateTime(GetSystem10msCount(), s_LastTime) <= 5)
		{
			GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(GetSystem10msCount(), s_LastTime) <= ((BatVol-6700)/10))
		{
			GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(GetSystem10msCount(), s_LastTime) > ((BatVol-6700)/10))
		{
			s_LastTime = GetSystem10msCount();
		}	
	}
	else												
	{
		if(CalculateTime(GetSystem10msCount(), s_LastTime) <= 5)
		{
			GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(GetSystem10msCount(), s_LastTime) <= 30)
		{
			GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(GetSystem10msCount(), s_LastTime) > 30)
		{
			s_LastTime = GetSystem10msCount();
		}
	}
	
	if(GetComTestFlag())	//调试模式
	{
		if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
		{
			GPIO_ToggleBits(LED_NET_TYPE,LED_NET_PIN);
			s_LastTime = GetSystem10msCount();
		}
	}
	else		//正常模式
	{
		if(p_sys->Type == NETTYPE_LORA)
		{
			if(GetLEDFlag())
			{
				GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
				LORA_SLEEP_MODE();
				delay_ms(100);
				s_LastTime2 = GetSystem10msCount();
				return ;
			}
			if(GetLoraState())
			{
				if(CalculateTime(GetSystem10msCount(), s_LastTime2) <= 10)	//LORA故障300ms闪烁
				{
					GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime2) <= 150)
				{
					GPIO_ResetBits(LED_NET_TYPE,LED_NET_PIN);
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime2) > 150)
				{
					s_LastTime2 = GetSystem10msCount();
				}					
			}
			else
			{
				GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);	
			}
		}
		else
		{
			if(GetTCModuleReadyFlag())	//连接上..常亮
			{
				BlinkTime = 1000;
			}
			else
			{
				BlinkTime = 30;
			}
			
			if(CalculateTime(GetSystem10msCount(), s_LastTime2) <= 10)	//连接中...闪烁
			{
				GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime2) <= BlinkTime)
			{
				GPIO_ResetBits(LED_NET_TYPE,LED_NET_PIN);
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime2) > BlinkTime)
			{
				s_LastTime2 = GetSystem10msCount();
			}			
		}
	}
}

void LED_NET_FLASH(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
}

