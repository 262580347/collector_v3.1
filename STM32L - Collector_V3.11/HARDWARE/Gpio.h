#ifndef __LED_H
#define __LED_H	 

#define		LED_POWER_PIN	GPIO_Pin_0
#define		LED_POWER_TYPE	GPIOA

#define		LED_NET_PIN		GPIO_Pin_1
#define		LED_NET_TYPE	GPIOA

#define		RUN_MODE_PIN		GPIO_Pin_11
#define		RUN_MODE_TYPE		GPIOA

#define		READ_RUN_MODE()		GPIO_ReadInputDataBit(RUN_MODE_TYPE, RUN_MODE_PIN)

void STM32_GPIO_Init(void);
void RunLED(unsigned short nMain10ms);
void RelayControl(unsigned short nMain100ms);
void WakeUp_GPIO_Init(void);
void LED_NET_FLASH(void);
void Stop_GPIO_Init(void);

void Stop_GPIO_Init_SIM(void);
#endif
