/**********************************
说明：传感器输入通道配置
	  
作者：关宇晟
版本：V2019.5.7
***********************************/
#include "main.h"
#include "map_in.h"

#define		ADDR_OFFSET	10
#define 	MAP_IN_BASE_ADDR	(EEPROM_BASE_ADDR + 10*EEPROM_BLOCK_SIZE)  	//系统配置存储区
#define 	COUNT_IN_A_SECTOR	20
#define		BUF_SIZE		64

typedef __packed struct
{
	unsigned char magic;
	unsigned short int length;
	unsigned char chksum;
}MAP_IN_TAG;

static unsigned int Check_area(unsigned int base_addr)
{
	MAP_IN_TAG tag;
	unsigned char chksum;
	unsigned char data[4];
	
	EEPROM_ReadBytes(base_addr, data, 4);

	tag.magic = data[0];
	tag.length = (data[1]) + ((data[2]) << 8);
	tag.chksum = data[3] ;
	
	if (tag.magic != 0x55)
	{
//		u1_printf("0x55\r\n");
		return MAP_IN_NO55;
	}
	
	if (tag.length > (MAP_IN_ITEM_SIZE - sizeof(MAP_IN_TAG)))
	{		
		return MAP_IN_LENGTH_ERR;
	}
	
	
//	chksum = Calc_Checksum((unsigned char *)(base_addr+sizeof(MAP_IN_TAG)), tag.length);
	chksum = EEPROM_CheckSum((base_addr+sizeof(MAP_IN_TAG)), tag.length);
	if (tag.chksum != chksum) 		
	{
//		u1_printf("chksum:%02X != tag.chksum:%02X\r\n", chksum, tag.chksum);
		return MAP_IN_CHECK_ERR;
	}
	
	return MAP_IN_NORMAL;
}

//获取指向输入配置信息的指针(没初始化配置的话,返回为NULL)



 MAP_IN_ITEM *get_map_in_item(unsigned int index)
{
	unsigned int addr;
	u8 ReturnVal = 0;
	MAP_IN_ITEM *item;
	static u8 s_Map_In_Buff[BUF_SIZE];
	
	item = NULL;
	if (index >= COUNT_IN_A_SECTOR) 
	{
		u1_printf(" 通道 (%d) 超范围\r\n", index);
		return item;
	}
	
	addr = MAP_IN_BASE_ADDR + index*MAP_IN_ITEM_SIZE;
	
	ReturnVal = Check_area(addr);
	if (ReturnVal == MAP_IN_NORMAL)
	{
		//返回实际设置值

//		EEPROM_ReadBytes(addr + sizeof(MAP_IN_TAG), &Map_data[BUF_SIZE*index], sizeof(MAP_IN_ITEM));//
		EEPROM_ReadBytes(addr + sizeof(MAP_IN_TAG), s_Map_In_Buff, sizeof(MAP_IN_ITEM));
		
		item = (MAP_IN_ITEM *)s_Map_In_Buff;//
		
		if(item->spec_len != 0)
		{
			EEPROM_ReadBytes(addr + sizeof(MAP_IN_TAG), s_Map_In_Buff, sizeof(MAP_IN_ITEM) + item->spec_len);		
			
			item = (MAP_IN_ITEM *)s_Map_In_Buff;
		}
		
	}
	
	else if (ReturnVal == MAP_IN_NO55) 
	{
		item = NULL;
	}
	else if (ReturnVal == MAP_IN_CHECK_ERR)
	{
		u1_printf(" 通道 (%d) 校验错误\r\n", index);
		item = NULL;
	}
	else if (ReturnVal == MAP_IN_LENGTH_ERR) 
	{
		u1_printf(" 通道 (%d) 长度错误\r\n", index);
		item = NULL;
	}
	
	return item;
}

//写入输入配置内容的程序接口
static 	unsigned char map_buf[BUF_SIZE*COUNT_IN_A_SECTOR];
unsigned int set_map_in_item(unsigned int index, MAP_IN_ITEM *item)
{
	int mod;

	unsigned int addr;
	static MAP_IN_TAG tag;
	MAP_IN_TAG *p_tag;
	
	p_tag = &tag;

	mod = index%COUNT_IN_A_SECTOR;
		
	addr = MAP_IN_BASE_ADDR + mod*MAP_IN_ITEM_SIZE;
	if (item)	//添加或修改通道配置
	{
		//添加或修改
		p_tag = (MAP_IN_TAG *)&map_buf[mod*BUF_SIZE];
		p_tag->magic = 0x55;
		p_tag->length = item->spec_len+sizeof(MAP_IN_ITEM);
		memcpy(&map_buf[mod*BUF_SIZE + sizeof(MAP_IN_TAG)], item, p_tag->length);
		p_tag->chksum = Calc_Checksum(&map_buf[mod*BUF_SIZE + sizeof(MAP_IN_TAG)], p_tag->length);
		
		addr = MAP_IN_BASE_ADDR + mod*MAP_IN_ITEM_SIZE;
		p_tag = (MAP_IN_TAG *)&map_buf[mod*BUF_SIZE];
		EEPROM_WriteBytes(addr, (unsigned char *)&map_buf[mod*BUF_SIZE], p_tag->length+sizeof(MAP_IN_TAG));
		
	}
	else	//删除通道
	{
		//删除配置
		EEPROM_EraseWords(index+ADDR_OFFSET);	
	}
	

	
	return 1;
}

