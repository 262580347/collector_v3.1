#include "485Sensor.h"
#include "main.h"

//	485传感器指令格式：
//	地址	功能码		寄存器地址		寄存器个数		CRC-L		CRC-H		嘉创
//	0x02	0x03		0x00 0x2A		0x00 0x01		0xA5		0xF1

//	地址	功能码		寄存器地址		寄存器个数		CRC-L		CRC-H		新普惠
//	0x01	0x03		0x00 0x00		0x00 0x01		0x84		0x0A

#define		ERROR_STATE_NORMAL			0x00
#define		ERROR_STATE_OVERTIME		0x01
#define		ERROR_STATE_SENSORERROR		0x02
#define		ERROR_STATE_CRCERROR		0x03
#define		ERROR_STATE_OVERDATA		0x04

#define		ERROR_485VALUE		0XEEEEEEEE
#define		ERROR_SENSOR_DATA	0XFFFF
#define		ERROR_SOIL_M		655.35
#define		SENSOR485_BAND_RATE	9600
#define 	CRC_SEED   0xFFFF   
#define 	POLY16 		0x1021  

#define		CMD_FUNCTION		0x06	//修改地址
#define		CMD_INQRIRE			0x09	//查询地址

#define		SAMPLING_INTERVAL	20

static  CHANNEL_DATA		SensorChannelData;

static unsigned char s_InputChannelCount = 0;

unsigned char g_485SensorGetFlag = FALSE;

static unsigned char s_SensorRunFlag = TRUE;	//测试模式下传感器循环读取

unsigned short g_ReportDataWaitingTime = 1;	//上报间隔(S)

void EXTI_Rainfall_Reset(void);

void SetSensorRunFlag(unsigned char isTrue)
{
	s_SensorRunFlag = isTrue;
}

unsigned char GetInputChannelCount(void)
{
	return s_InputChannelCount;
}

float GetSensorData(unsigned char SensorNum)
{
	return SensorChannelData.ChannelData[SensorNum];
}

unsigned char Uart_Send_StartMeasure(unsigned char SensorAddr)
{
	static unsigned char data[8];
	unsigned short CRCVal = 0;
	u16 nMain10ms = 0, i;
	u8 Err = 0;
	
	data[0] = SensorAddr;
	data[1] = 0x03;
	data[2] = 0x25;
	data[3] = 0x00;	
	data[4] = 0x00;
	data[5] = 0x01;
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;	
	
	delay_ms(100);
	Uart_Send_Data(SENSOR485_COM, data, 8);
	
	nMain10ms = GetSystem10msCount();
	while(CalculateTime(GetSystem10msCount(), nMain10ms) < 30)
	{
		if(g_Uart3RxFlag == TRUE)
		{
			if((g_USART3_RX_CNT) > 5 && (SensorAddr == g_USART3_RX_BUF[0]))
			{
				Err = 1;
			}
			for(i=0; i<g_USART3_RX_CNT; i++)
			{
				u1_printf("%02X ",g_USART3_RX_BUF[i]);
			}
			u1_printf("\r\n");
			Clear_Uart3Buff();
			break;
		}	

		TaskForDebugCOM(); //解决等待时间过长串口 No Ack		
	}
	
	return Err;
}



void Uart_Send_485CMD(MAP_IN_ITEM *item)
{
	static unsigned char data[8] = {0x02, 0x03, 0x00, 0x00, 0x00, 0x01, 0xA5, 0XF1};
	unsigned short CRCVal = 0, i;
	
	data[0] = item->in_param[2];
	
	if(item->in_param[6])	// 04功能码获取数据
	{
		data[1] = item->in_param[6];
	}
	else
	{
		data[1] = 0x03;
	}
	
	data[2] = item->in_param[3];
	data[3] = item->in_param[4];	
	
	if(item->in_param[7])		//如果填写了读取字节数
	{
		data[4] = 0x00;
		data[5] = item->in_param[7];
	}
	else if(item->data_id == H2O_NH3_ID || item->data_id == H2O_TURBIDITY_ID || item->data_id == H2O_CL2_ID || item->data_id == ORP_POTENTIOMETER_ID || item->data_id == YMSD_ID)
	{
		data[4] = 0x00;
		data[5] = 0x02;
	}
	else if(item->data_id == RJY_ID)	//Dissolved Oxygen 4字节
	{
		data[4] = 0x00;
		data[5] = 0x04;
	}
	else if(item->data_id == CHLOROPHYLL_ID)	//Chlorophyll 5字节
	{
		data[4] = 0x00;
		data[5] = 0x05;
	}
	else
	{
		data[4] = 0x00;
		data[5] = 0x01;
	}
	
	
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;	
	
	USART3_DMA_Send(data, 8);
}

void Uart_Set_485Addr(USART_TypeDef* USARTx, unsigned char sAddr, unsigned char dAddr)
{
	static unsigned char data[8] = {0x02, 0x06, 0x20, 0x00, 0x00, 0x01, 0xC2, 0X38};
	unsigned short CRCVal = 0;
	
	data[0] = sAddr;	//源地址
	data[1] = 0x06;		//修改地址功能码
	data[2] = 0x00;		//寄存器地址高位
	data[3] = 0x00;		//寄存器地址低位
	data[4] = 0x00;		//目标地址高位
	data[5] = dAddr;	//修改后的地址
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;
	USART3_DMA_Send(data, 8);	
}

void Uart_Inquire_485Addr(USART_TypeDef* USARTx)
{
	static unsigned char data[8] = {0x00, 0x06, 0x20, 0x00, 0x00, 0x00, 0xC2, 0X38};
	unsigned short CRCVal = 0;
	
	data[0] = 0x00;	//源地址
	data[1] = 0x09;		//查询地址功能码
	data[2] = 0x00;		//寄存器地址高位
	data[3] = 0x01;		//寄存器地址低位
	data[4] = 0x00;		//目标地址高位
	data[5] = 0x00;	//修改后的地址
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;
	USART3_DMA_Send(data, 8);	
}


unsigned int Wait485SensorData(MAP_IN_ITEM *item, unsigned char *ErrorFlag, float *f_Val1, float *f_Val2, unsigned char *DataNum)
{
	unsigned short s_LastTime = 0, CRCVal = 0;
	unsigned int Result = 0;
	unsigned char data[100], i, Num = 0, Count = 0;
	
	s_LastTime = GetSystem10msCount();
	memset(data, 0 ,sizeof(data));
	Count = 0;

	while(CalculateTime(GetSystem10msCount(), s_LastTime) < 80)
	{
		if(g_Uart3RxFlag == TRUE)
		{
			g_Uart3RxFlag = FALSE;
			
			memcpy(&data[Count], g_USART3_RX_BUF, g_USART3_RX_CNT);	
			Count += g_USART3_RX_CNT;
			if(Count > 100)
			{
				break;
			}	
			Clear_Uart3Buff();
		}		
		
		if(Count == (5 + data[2]))
		{
			break;
		}
		IWDG_ReloadCounter();	
		TaskForDebugCOM(); //解决等待时间过长串口 No Ack
	}
	
	if(Count != (5 + data[2]) && Count > 0)
	{
		u1_printf(" 接收长度错误: ");
		for(i=0; i<Count; i++)
		{
			u1_printf("%02X ",data[i]);
		}
		u1_printf("\r\n");
	}
		
	if(Count == 0)
	{
		Result = ERROR_485VALUE;
		*ErrorFlag = ERROR_STATE_OVERTIME;
		return Result;
	}	
//			if(item->data_id == KQW_ID || item->data_id == KQS_ID || item->data_id == GZD_ID || item->data_id == AIR_PRESS_ID || item->data_id == H2O_NH3_ID || \
//				item->data_id == H2O_TURBIDITY_ID || item->data_id == H2O_CL2_ID || item->data_id == RJY_ID || item->data_id == SOIL_M_ID || item->data_id == SOIL_WATER_POTENTIAL || item->data_id == TRW_ID || item->data_id == CHLOROPHYLL_ID)
		switch(item->data_id)
		{
			case TRW_ID:
				CRCVal = Calculate_CRC16(data, 7);
				if(CRCVal == ((data[8] << 8) + data[7]))
				{
					Result = (float)((data[5] << 8) + data[6]);
					*f_Val1 = Result/100.0 -20;

				}
				else
				{
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}		
			break;
			
			case TRS_ID:
				CRCVal = Calculate_CRC16(data, 7);
				if(CRCVal == ((data[8] << 8) + data[7]))
				{
					Result = (float)((data[3] << 8) + data[4]);
					*f_Val1 = Result/10.0;

				}
				else
				{
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}		
			break;
				
			case KQW_ID:
			case KQS_ID:
			case GZD_ID:
			case AIR_PRESS_ID:
			case DEW_POINT:		//露点
			case H2O_NH3_ID:
			case H2O_TURBIDITY_ID:
			case H2O_CL2_ID:
			case RJY_ID:
			case SOIL_M_ID:
			case SOIL_WATER_POTENTIAL:
	////临时注释		case TRW_ID:
			case CHLOROPHYLL_ID:	
				
				Num = data[2];
				*DataNum = Num;
			
				if(Num == 4)	//4字节浮点数
				{
					CRCVal = Calculate_CRC16(data, 7);
					if(CRCVal == ((data[8] << 8) + data[7]))
					{

						if((data[3] << 24) + (data[4] << 16) + (data[5] << 8) + data[6] == ERROR_485VALUE)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
							Result = ERROR_485VALUE;
						}
						else
						{
							memcpy(f_Val1, (void *)&data[3], sizeof(float));
							*ErrorFlag = ERROR_STATE_NORMAL;
						}
//						u1_printf("%02X %02X %02X %02X\r\n", data[3] ,data[4] ,data[5] ,data[6]); 
					}
					else
					{
//						u1_printf("\r\n ID:%02X:HY Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 8)	//Dissolved Oxygen
				{
					CRCVal = Calculate_CRC16(data, 11);
					if(CRCVal == ((data[12] << 8) + data[11]))
					{
						memcpy(f_Val1, (void *)&data[3], sizeof(float));	
						memcpy(f_Val2, (void *)&data[7], sizeof(float));	
						*ErrorFlag = ERROR_STATE_NORMAL;
						u1_printf("%2.2f %2.3f%% ", *f_Val1, *f_Val2); 
					}
					else
					{
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 10)	//Chlorophyll
				{
					CRCVal = Calculate_CRC16(data, 13);
					if(CRCVal == ((data[14] << 8) + data[13]))
					{
						memcpy(f_Val1, (void *)&data[3], sizeof(float));	
						memcpy(f_Val2, (void *)&data[7], sizeof(float));	
						*ErrorFlag = ERROR_STATE_NORMAL;
						u1_printf("%2.2f %2.3fug/L", *f_Val1, *f_Val2); 
					}
					else
					{
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 2)	//其他传感器2字节
				{
					CRCVal = Calculate_CRC16(data, 5);
					if(CRCVal == ((data[6] << 8) + data[5]))
					{
						Result = (float)((data[3] << 8) + data[4]);
						*f_Val1 = Result;
						if(Result == ERROR_SENSOR_DATA)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
						}
					}
					else
					{
//					u1_printf("\r\n ID:%02X:Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else
				{
//					u1_printf(" error num:%d\r\n", Num);
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}
			
			break;
			
			default:
				
				Num = data[2];
				*DataNum = Num;
			
				if(Num <= 4)
				{
					Result = 0;
					CRCVal = Calculate_CRC16(data, Num+3);
					if(CRCVal == ((data[4+Num] << 8) + data[3+Num]))
					{
						for(i=0; i<Num; i++)
						{
							if(item->data_id == ORP_POTENTIOMETER_ID)
							{
								Result = ((data[3] << 8) + data[4]);
								*f_Val1 = (float)Result;
								
								if(Result > 10000)	//是负数
								{
									Result = 0xFFFF - Result;
									*f_Val1 = 0.0 - (float)Result;
								}
							}
							else if(item->data_id == YMSD_ID)		//叶面湿度传感器						
							{
								Result = ((data[3] << 8) + data[4]);
								*f_Val1 = Result;
							}
							else 
							{
								Result += data[3+i] << (8*(Num- i - 1));
								*f_Val1 = Result;
							}
						}
						if(Result == ERROR_SENSOR_DATA)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
						}
					}
					else
					{
//						u1_printf("\r\n ID:%02X:Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else
				{
//					u1_printf("\r\n ID:%02X:Num Error", SensorID);
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}								
			break;
		}

	
//		if(item->data_id == PH_ID || item->data_id == RJY_ID || item->data_id == AIR_ION)
//		{
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50) 	//PH 延时长
//			{
//				s_LastTime = GetSystem10msCount();					
////				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
//				Result = ERROR_485VALUE;
//				*ErrorFlag = ERROR_STATE_OVERTIME;
//				break;
//			}	
//		}
//		else if(item->data_id == CHLOROPHYLL_ID)
//		{
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 150) 	//Chlorophyll
//			{
//				s_LastTime = GetSystem10msCount();					
////				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
//				Result = ERROR_485VALUE;
//				*ErrorFlag = ERROR_STATE_OVERTIME;
//				break;
//			}	
//		}
//		else
//		{
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 25) 
//			{
//				s_LastTime = GetSystem10msCount();					
////				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
//				Result = ERROR_485VALUE;
//				*ErrorFlag = ERROR_STATE_OVERTIME;
//				break;
//			}		
//		}		
//		IWDG_ReloadCounter();	
//		TaskForDebugCOM(); //解决等待时间过长串口 No Ack
//	}		
	
//	Clear_Uart3Buff();
	
	return Result;
}
//等待慧云传感器修改地址指令应答
unsigned char Wait485SensorAck(void)
{
	unsigned short s_LastTime = 0, CRCVal = 0;
	unsigned int Result = 0;
	unsigned char data[9], i, SensorID, Function;
	
	s_LastTime = GetSystem10msCount();
	while(1)
	{
		if(g_Uart3RxFlag == TRUE)
		{
			g_Uart3RxFlag = FALSE;
			for(i=0; i<9; i++)
			{
				data[i] = g_USART3_RX_BUF[i];
			}
			
			SensorID = data[0];
			Function = data[1];
			
			if(Function == CMD_FUNCTION)		//修改地址功能码
			{
				CRCVal = Calculate_CRC16(data, 6);
				if(CRCVal == ((data[7] << 8) + data[6]))
				{
					u1_printf("\r\n Sensor Addr (0x%02X) to (0x%02X) succeed!\r\n", data[0], data[5]);		
				}
				else
				{
					u1_printf("\r\n ID:%02X :CRC error", SensorID);
					Result = ERROR_485VALUE;
					for(i=0; i<8; i++)
					{
						u1_printf(" %02X", g_USART3_RX_BUF[i]);
					}
					u1_printf("\r\n");
				}
			}
			else if(Function == CMD_INQRIRE)		//查询地址功能码
			{
				CRCVal = Calculate_CRC16(data, 6);
				if(CRCVal == ((data[7] << 8) + data[6]))
				{
					u1_printf("\r\n RS485 Addr:%02X\r\n", data[2]);
				}
				else
				{
					u1_printf("\r\n Inquire Addr Error: ");
					Result = ERROR_485VALUE;
					for(i=0; i<8; i++)
					{
						u1_printf(" %02X", g_USART3_RX_BUF[i]);
					}
					u1_printf("\r\n");
				}
			}
			break;
		}
		
		if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
		{
			s_LastTime = GetSystem10msCount();					
			u1_printf("\r\n Change Addr Over time\r\n");
			Result = ERROR_485VALUE;
			break;
		}
	}		
	Clear_Uart3Buff();
	return Result;
}
//获取传感器数据
float Get485SensorValue(MAP_IN_ITEM *item, unsigned char *Err)
{
	volatile float f_Result = 0.0, f_Return = 0.0;
	uint32 ErrValue = ERROR_485VALUE;
	float f_Val1 = 0, f_Val2 = 0, X1, X2;
	unsigned char ErrorFlag = 0, DataNum = 0 ;
	
	double pressure = 101.3;
	double Phmg= 0.0;
	double T = 0.0;
	double u = 0.0;

	memcpy((uint8 *)&f_Return, (uint8 *)&ErrValue, sizeof(uint32));
	*Err = ERROR_STATE_NORMAL;
	
	Uart_Send_485CMD(item);
	
	f_Result = Wait485SensorData(item, &ErrorFlag, &f_Val1, &f_Val2, &DataNum);
	
	if(ErrorFlag == ERROR_STATE_OVERTIME)
	{
//		u1_printf(" Err1 ");
		*Err = ERROR_STATE_OVERTIME;
		return f_Return;
	}
	else if(ErrorFlag == ERROR_STATE_SENSORERROR)
	{
		u1_printf(" Err2 ");
		*Err = ERROR_STATE_SENSORERROR;
		return f_Return;
	}
	else if(ErrorFlag == ERROR_STATE_CRCERROR)
	{
		u1_printf(" Err3 ");
		*Err = ERROR_STATE_CRCERROR;
		return f_Return;
	}
	
	if(item->in_param[5])
	{
		if(DataNum == 2)	//整型数
		{
			
		}
		else	//浮点数
		{
			f_Result = f_Val1;
		}
		
		switch(item->in_param[5])
		{
			case 1:
				f_Result = f_Result*0.001;
			break;
			case 2:
				f_Result = f_Result*0.01;
			break;
			case 3:
				f_Result = f_Result*0.1;
			break;
			case 4:
				f_Result = f_Result*0.5;
			break;
			case 5:
				f_Result = f_Result*1;
			break;
			case 6:
				f_Result = f_Result*2;
			break;
			case 7:
				f_Result = f_Result*5;
			break;
			case 8:
				f_Result = f_Result*10;
			break;
			case 9:
				f_Result = f_Result*20;
			break;
			case 10:
				f_Result = f_Result*50;
			break;
			case 11:
				f_Result = f_Result*100;
			break;
			case 12:
				f_Result = f_Result*500;
			break;
			case 13:
				f_Result = f_Result*1000;
			break;
			default:
				u1_printf("Err");
				f_Result = f_Result;
			break;
		}
	}
	else
	{
		switch(item->data_id)
		{
		//		特殊处理
			case TRW_ID:
			case TRS_ID:
				f_Result = f_Val1;
			break;
////临时注释			case TRW_ID:		//Soil Temperature
//				if(item->in_param[4] == TRW_ID)	
//				{
//					f_Result = f_Val1;
//				}
//				else
//				{
//					if(f_Result < 8000)		//注意  Soil Temperature与Liquid Level的算法不同，Soil Temperature正负以8000为临界，Liquid Level以0x8000为临界
//					{
//						f_Result = f_Result/10.0;
//					}
//					else		//负数
//					{
//						f_Result = (0xffff - f_Result + 1)*0.1;
//						f_Result = -f_Result;
//					}	
//				}			
//			break;	

			case RJY_ID:	//Dissolved Oxygen转换公式
				//数学函数占用flash空间过多，暂时屏蔽
		//			T = 273.15 + f_Val1;
		//			X1 = -173.4292+249.6339*(100.0/T)-21.8492*(T/100.0)+(log(T/100.0))*143.3483;
		//			X1 = exp(X1);
		//			u = 8.10765 - (1750.286/ (235 + f_Val1));// u� = log u
		//			u = pow(10, u); 
		//			Phmg = pressure*760.0/101.325; 
		//			X2  =  ((Phmg - u)/(760 - u)); 
		//		
		//			f_Result = f_Val2*X1*X2*1.4276;
				u1_printf(" 溶解氧传感器未添加，请联系GYS处理\r\n");
				if(f_Result < 0 && f_Result > -1)	//接近于0时有可能出现负值
				{
					f_Result = 0;
				}
				
			break;
			
			case ORP_POTENTIOMETER_ID:	//氧化还原
				f_Result = f_Val1;
			break;
			
			case CHLOROPHYLL_ID:		//Chlorophyll
				f_Result = f_Val2;
			
				if(f_Result < 0 && f_Result > -1)	//接近于0时有可能出现负值
				{
					f_Result = 0;
				}
			break;
			
			case YW_ID:					//Liquid Level		
				f_Result = f_Result*0.0005;		//星仪Liquid Level传感器
			break;
		//		读数不处理			
			case SOIL_YF_ID:			//Soil Salinity值
			
				//Nothing
			break;
			case SOIL_WATER_POTENTIAL:	//土壤水势传感器		
				f_Result = -f_Val1;
			break;
		//		读数/10			
//临时注释			case TRS_ID:		//Soil Humidity
			case FX_ID:			  //Wind Direction
			case FS_ID:			  //Wind Speed
			case O2_ID:
			case CO2_PER_ID:
			case YMSD_ID:
				f_Result = f_Result*0.1;
			break;	
		//		慧云传感器
			case GZD_ID:			//Illuminance

				f_Result = f_Val1;					
				
			break;

			case AIR_PRESS_ID:			//Atmospheric Pressure			
			
				f_Result = f_Val1;
					

			break;
			
			case KQW_ID:			//Air Temperature

				
				f_Result = f_Val1;

			break;		
			
			case KQS_ID:			//空气湿度				
	
				f_Result = f_Val1;

			break;
				
			case DEW_POINT:	//露点
				if(f_Val1 < 8000)		//小于400即可，负值时大于0xf000
				{
					f_Result = f_Val1/10.0;
				}
				else		//负数
				{
					f_Result = (0xffff - f_Val1)/10.0;
					f_Result = -f_Result;
				}
			break;
			
			case H2O_TURBIDITY_ID:
			case H2O_NH3_ID:
			case H2O_CL2_ID:
				f_Result = f_Val1;
				f_Result = encode_float(f_Result);	
			break;
			
			case SYL_ID:		//Water Pressure  0~1.6Mpa量程
				f_Result = 1.6*f_Result/2.0;		//星仪压力计	*1000 显示为Kpa  20190703 gys
			break;
		//		读数/100		
			case PH_ID:			//PH Value
			case DDL_ID:		//Water Electric
			case SOIL_EC_ID:			//Soil Electric
			case NH3_NH4_ID:
				f_Result = f_Result*0.01;
			break;
		//Soil Tension    电流型或SDI-12		
			case SOIL_M_ID:
				if(item->in_param[4] == SOIL_WATER_POTENTIAL)		
				{
					f_Result = -f_Val1;
				}
				else
				{
					f_Result = f_Result*0.01;
				}
			break;	
			
			default:
			
			break;

		}	
	}
	
	f_Result = f_Result + item->compensation;	//修正值
	return f_Result;
}


void Init_485Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR485_ENABLE_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SENSOR485_ENABLE_GPIO_TYPE, &GPIO_InitStructure);	
	
	SENSOR485_ENABLE();

}

void Deinit_485Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR485_ENABLE_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SENSOR485_ENABLE_GPIO_TYPE, &GPIO_InitStructure);	
	
	SENSOR485_DISABLE();
	
}


unsigned char Get_485Sensor_Data(unsigned char ChannelNum)
{
	MAP_IN_ITEM *item;
	uint32 error_mark = 0xEEEEEEEE;
	u32 Pluse1 = 0, Pluse2 = 0;
	u8 i, RertyCount = 2, Err = 0;
	
	if(	hwl_input_init(ChannelNum, FALSE) )
	{			
		item = get_map_in_item(ChannelNum);
		
		//根据通道读取传感器数据，数据与通道一一对应
		
		if((item->data_id != JYL_ID && item->data_id != LL_ID ) || item->in_param[0] == 3)
		{
			SensorChannelData.ChannelData[ChannelNum] = Get485SensorValue(item, &Err);
			if(item->data_id == YW_ID)	//不同传感器重发次数
			{
				RertyCount = 3;
			}
			else if(item->data_id == CHLOROPHYLL_ID || item->data_id == YMSD_ID)
			{
				RertyCount = 3;
			}
			else
			{
				RertyCount = 2;
			}
			for(i=0; i<RertyCount; i++)
			{
				if(Err > 0)
				{
					Clear_Uart3Buff();
					delay_ms(150);
					SensorChannelData.ChannelData[ChannelNum] = Get485SensorValue(item, &Err);
				}
				else
				{
					break;
				}
			}
		}	
		
		if(item->data_id == LL_ID || item->data_id == JYL_ID )
		{			
//			ShowSensorName(item->data_id, FALSE, SensorChannelData.ChannelData[ChannelNum]);
		}
		else
		{
			ShowSensorName(item->data_id, TRUE, SensorChannelData.ChannelData[ChannelNum]);
		}
		
		
		switch(item->data_id)
		{													
			case JYL_ID:	//RainFall
				if(item->in_param[0] == 0)		//通道1
				{
					SensorChannelData.ChannelData[ChannelNum] = GetRainfall();
					SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.2;					
					u1_printf("\r\n  (1)");				
				}
				else if(item->in_param[0] == 1)	//通道2
				{
					SensorChannelData.ChannelData[ChannelNum] = GetRainfall2();		
					SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.2;						
					u1_printf("\r\n  (2)");						
				}
				else if(item->in_param[0] == 3)	//通道2
				{
					SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum];					
					u1_printf("\r\n (RS485)");						
				}
				
				u1_printf(" RainFall Pluse: %.1f\r\n", SensorChannelData.ChannelData[ChannelNum]);
			break;
				
			case LL_ID:	//Flow
				if(item->in_param[0] == 0)		//通道1
				{
					SensorChannelData.ChannelData[ChannelNum] = GetRainfall();
					u1_printf(" (1)");	
				}
				else if(item->in_param[0] == 1)	//通道2
				{
					SensorChannelData.ChannelData[ChannelNum] = GetRainfall2();	
					u1_printf(" (2)");
					
				}
				else if(item->in_param[0] == 2)	//脉冲联动
				{
					Pluse1 = GetRainfall();	
					Pluse2 = GetRainfall2();	
					
					if(Pluse1 == Pluse2)	//仅当两通道脉冲数相等
					{
						SensorChannelData.ChannelData[ChannelNum] = Pluse1;
					}
					else if(Pluse1 > Pluse2)
					{
						SensorChannelData.ChannelData[ChannelNum] = Pluse2;
					}
					else if(Pluse1 < Pluse2)
					{
						SensorChannelData.ChannelData[ChannelNum] = Pluse1;
					}
					
					u1_printf(" (1)(2)");
					
				}
				
				switch(item->in_param[5])	//每个脉冲对应的Flow数值是多少。
				{
					case 1:
						SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.001;	  //每脉冲1 M3
						break;
					case 2:
						SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.01;   //每脉冲10 M3
						break;
					case 3:
						SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.1;   //每脉冲10 M3
						break;
					case 4:
						SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum]*0.5;   //每脉冲10 M3
						break;
					case 5:
						SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum];   //每脉冲10 M3
						break;					
				}			
				u1_printf("Flow Pluse: %.1f\r\n", SensorChannelData.ChannelData[ChannelNum]);
				
			break;	
			
		}
		
		if(Err > 0)
		{		
			SetLogErrCode(LOG_CODE_SENSORERR);
			if(SensorChannelData.SensorOKStatus[ChannelNum])
			{
				if(SensorChannelData.ErrorCount[ChannelNum] >= 3)
				{
					memcpy((uint8 *)&SensorChannelData.ChannelData[ChannelNum], (uint8 *)&error_mark, sizeof(uint32));
					SensorChannelData.SensorOKStatus[ChannelNum] = FALSE;
				}				
				else
				{
					SensorChannelData.ErrorCount[ChannelNum]++;
					SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.LastChannelData[ChannelNum];
				}
			}
			else
			{
				memcpy((uint8 *)&SensorChannelData.ChannelData[ChannelNum], (uint8 *)&error_mark, sizeof(uint32));
				
			}		
		}
		else	//存储上一次正常数据
		{
			SensorChannelData.LastChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum];
			SensorChannelData.ErrorCount[ChannelNum] = 0;
			SensorChannelData.SensorOKStatus[ChannelNum] = TRUE;
		}
		
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static void SpecialSensorStartCMD(void)
{
	u8 i, Addr = 0, Err = 0;
	
	if(GetRJYSensorFlag(&Addr))
	{
		for(i=0; i<10; i++)
		{
			Err = Uart_Send_StartMeasure(Addr);
			if(!Err)
			{
				Clear_Uart3Buff();
				delay_ms(100);
			}
			else
			{
				u1_printf("\r\n Start Dissolved Oxygen Measure Fail\r\n");
				break;
			}
			IWDG_ReloadCounter();	
			TaskForDebugCOM();		//串口测试
		}		
		if(i == 10)
		{
			u1_printf("\r\n Start Dissolved Oxygen Measure Fail\r\n");
		}
	}
	
	if(GetChlorophyllSensorFlag(&Addr))
	{
		for(i=0; i<10; i++)
		{
			Err = Uart_Send_StartMeasure(Addr);
			if(!Err)
			{
				Clear_Uart3Buff();
				delay_ms(100);
			}
			else
			{
				u1_printf("\r\n Start Chlorophyll Measure Fail\r\n");
				break;
			}
			IWDG_ReloadCounter();	
			TaskForDebugCOM();		//串口测试
		}		
		if(i == 10)
		{
			u1_printf("\r\n Start Chlorophyll Measure Fail\r\n");
		}
	}
}

void Sensor485_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime = 0;
	static u8 s_State = 1, RunOnce = FALSE, s_State2 = 1, s_ChannelNum = 0, s_IsTrue = 0, s_WaitStartTime = 0;
	static u16 s_Data_interval = 0, s_LastGetSensorDataTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	u8 i, Addr = 0;
	SYSTEMCONFIG *p_sys;
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		
		for (i=0;i<HWL_INPUT_COUNT;i++)
		{
			if(hwl_input_init(i, TRUE))		//读取输入通道配置信息
			{
				s_InputChannelCount++;		
			}
		}
		delay_ms(10);
		p_sys = GetSystemConfig();
		if(p_sys->State_interval == 0)
		{
			u1_printf("\r\n Wait Interval:%ds\r\n", g_ReportDataWaitingTime);
		}
		else
		{
			u1_printf("\r\n Wait Interval:%ds\r\n", p_sys->State_interval);
			g_ReportDataWaitingTime = p_sys->State_interval;
			
		}
		
		if(g_ReportDataWaitingTime > 300)//最大等待时长为300s，过长的等待没有意义，不如直接保持长连接
		{
			g_ReportDataWaitingTime = 300;
		}
			
		if(s_InputChannelCount)
		{
			u1_printf("\r\n Collector Init Finish,Channel Num:%d...\r\n", s_InputChannelCount);
		}
		else
		{
			g_485SensorGetFlag = TRUE;
		}
	}
	
	if(s_InputChannelCount == 0)
	{
		return;
	}
	
	if(g_PluseChannel_1_Flag || g_PluseChannel_2_Flag)	//脉冲型传感器入口
	{
		RainfallSensorProcess();
	}

	p_sys = GetSystemConfig();
	
	if(p_sys->LowpowerFlag == 1)
	{
		if(CalculateTime(GetSystem10msCount(), s_LastGetSensorDataTime) >= g_ReportDataWaitingTime*100)
		{
			s_LastGetSensorDataTime = GetSystem10msCount();
			Clear_Flag();	//开启传感器测量
			g_PowerDetectFlag = FALSE;
		}
	}
	
	if(GetComTestFlag() && s_SensorRunFlag == TRUE)		//测试模式，以上报间隔显示传感器数据
	{
		switch(s_State2)
		{
			case 1:			//开启传感器电源，初始化485串口
				Init_485Port();	
				USART3_Config(SENSOR485_BAND_RATE);
				s_LastTime = GetSystem10msCount();
				p_sys = GetSystemConfig();
				s_Data_interval = g_ReportDataWaitingTime;
												
				if(GetChlorophyllSensorFlag(&Addr))
				{
					s_WaitStartTime = 7;
					if(s_Data_interval < s_WaitStartTime)
					{
						s_Data_interval = 30;					
					}
					s_State2 = 2;
				}
				else if(GetRJYSensorFlag(&Addr))
				{
					s_WaitStartTime = 2;
					if(s_Data_interval < s_WaitStartTime)
					{
						s_Data_interval = 30;					
					}
					s_State2 = 2;
				}
				else
				{
					s_State2 = 3;
				}
				u1_printf("\r\n Data Interval:% ds\r\n", s_Data_interval);
			break;
			
			case 2:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_WaitStartTime*100)
				{
					u1_printf(" Start Measure\r\n");
					SpecialSensorStartCMD();
					s_State2 = 3;
				}				
			break;
			
			case 3:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_Data_interval*100) //延时等待传感器数据
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n[%02d:%02d:%02d]\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					Clear_Uart3Buff();
					s_ChannelNum = 0;
					s_State2++;
					s_LastTime = GetSystem10msCount();
				}	
			break;
			
			case 4:		//间隔读取传感器数据输出
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= SAMPLING_INTERVAL)
				{
					s_IsTrue = Get_485Sensor_Data(s_ChannelNum++);
					if(s_IsTrue)
					{
						s_LastTime = GetSystem10msCount();
					}				
				}
				
				if(s_ChannelNum >= HWL_INPUT_COUNT)
				{
					s_ChannelNum = 0;
					s_State2++;
				}
			break;
				
			case 5:
				s_State2 = 3;
				s_LastTime = GetSystem10msCount();
			break;
		}
	}	
	else 	//正常模式
	{
		if(g_485SensorGetFlag == FALSE)
		{
			switch(s_State)
			{
				case 1:			//开启传感器电源，初始化485串口
					Init_485Port();	
					USART3_Config(SENSOR485_BAND_RATE);
					s_LastTime = GetSystem10msCount();
					
					if(GetChlorophyllSensorFlag(&Addr))
					{
						s_WaitStartTime = 7;
						if(s_Data_interval < s_WaitStartTime)
						{
							s_Data_interval = 30;						
						}
						s_State = 2;
					}
					else if(GetRJYSensorFlag(&Addr))
					{
						s_WaitStartTime = 2;
						if(s_Data_interval < s_WaitStartTime)
						{
							s_Data_interval = 20;						
						}
						s_State = 2;
					}
					else
					{
						s_State = 3;
					}
					
				break;
				
				case 2:
					if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_WaitStartTime*100)
					{
						u1_printf(" Start Measure\r\n");
						SpecialSensorStartCMD();
						s_State = 3;
					}
				break;
				
				case 3:
					if(CalculateTime(GetSystem10msCount(), s_LastTime) >= g_ReportDataWaitingTime*100 + 20) //延时等待传感器数据
					{
						Clear_Uart3Buff();
						RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
						u1_printf("\r\n [%0.2d:%0.2d:%0.2d]----Original Data:----\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);					
						s_State++;
						s_LastTime = GetSystem10msCount();
						s_ChannelNum = 0;
					}	
				break;
					
				case 4:		//输出传感器原始数据
					if(CalculateTime(GetSystem10msCount(), s_LastTime) >= SAMPLING_INTERVAL)
					{
						s_IsTrue = Get_485Sensor_Data(s_ChannelNum++);
						if(s_IsTrue)
						{
							s_LastTime = GetSystem10msCount();
						}
					}
					
					if(s_ChannelNum >= HWL_INPUT_COUNT)
					{
						s_ChannelNum = 0;
						p_sys = GetSystemConfig();
	
						if(p_sys->LowpowerFlag == 1)
						{
							s_State = 3;
							g_485SensorGetFlag = TRUE;
						}
						else if(p_sys->LowpowerFlag == 0)
						{
							s_State++;
						}
						
					}
				break;
				
				case 5:		//关闭传感器电源和485串口
					Deinit_485Port();
					USART_Cmd(USART3, DISABLE);
					USART_DeInit(USART3);
					s_State = 1;
					g_485SensorGetFlag = TRUE;
				break;
				
			}

		}
	}
}













