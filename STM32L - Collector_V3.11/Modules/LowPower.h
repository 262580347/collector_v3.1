#ifndef  _LOW_POWER_H_
#define  _LOW_POWER_H_

#define		MEASUREMENT_INTERVAL  1 

#define		STOP_TIME			10

#define		ERR_STOP_TIME		2

extern unsigned char g_RTCAlarmFlag,  g_ExtiFlag;
	
void EnterStopMode(unsigned char Second);
void EnterStopMode_SIM(unsigned char Second);	

void Clear_Flag(void);

void Exit_Awake_Process(unsigned short g_nMain10ms);

void LowPower_Process(void);
	
extern unsigned char  g_ExtiRainfallFlag, g_ExtiRainfallFlag2;

void SetStopModeFlag(unsigned char isTrue);

unsigned char GetStopModeFlag(void);

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime);

void StopModeProcess(void);

#endif
