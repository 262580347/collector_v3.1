#ifndef		_RAINFALL_H_
#define		_RAINFALL_H_

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned int			PluseCount1;
	unsigned int			PluseCount2;
}PLUSE_SAVE;

unsigned int GetRainfall(void);

unsigned int GetRainfall2(void);

void ClearPluseCount1(void);

void ClearPluseCount2(void);

void RainfallSensorProcess(void);

void InitPluseCount(void);

void SetUnionPluse(unsigned char IsTrue);

#endif
