/**********************************
说明：低功耗传感器主函数
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "main.h"
#include "sys.h"

int main(void)
{
	SYSTEMCONFIG *p_sys;

	SysInit();	//系统外设、参数初始化
	
	p_sys = GetSystemConfig();
		
	while (1)
	{					
		Updata_IWDG(GetSystem100msCount());	//看门狗
		
		TaskForDebugCOM();		//串口测试

		if(GetComTestFlag() == FALSE)		//调试关闭
		{			
			if(p_sys->Type == NETTYPE_LORA)
			{
				LoraProcess(GetSystem10msCount());	//LORA 初始化
				
				TCProtocolForLoraProcess();
			}
//				else if(p_sys->Type == NETTYPE_2G)	
//				{
//					SIM800CProcess(GetSystem10msCount());//SIM800C直接上报
//				}
			else if(p_sys->Type == NETTYPE_GPRS)	
			{
				GPRS_Process(GetSystem10msCount());//GPRS
			}
			else if(p_sys->Type == NETTYPE_4G)	
			{
				SIM7600CEProcess(GetSystem10msCount());//4G
			}

		}	
		else
		{
			if(p_sys->Type == NETTYPE_LORA)
			{		
				TCProtocolForLoraProcess();
			}
		}
		
		TCProtocolProcess();
		
		RunLED(GetSystem10msCount());	
		
		Power_Vol_Detect(GetSystem10msCount());	//ADC电池电压检测

		StopModeProcess();				//停止模式入口
		
		LowPower_Process();				//唤醒后其他外设初始化
		
		Sensor485_Process(GetSystem10msCount());	//485传感器进程
	}
}


