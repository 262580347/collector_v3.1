/**********************************
说明：LORA通信模块，使用500ms唤醒间隔，TXD，AUX推挽输出,RXD 上拉输入
	  FEC开启，20dBm发射功率，2.4K空中速率
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "Lora.h"
#include "main.h"

#define	MAX_DATA_BUF	256

#define 	LORA_BUFF_MAX	250

#define		PACK_INIT_NUM	255
//#define	LORA_SPEED_INIT 	0x3A	//2.4K
#define	LORA_SPEED_INIT 	0x3D		//19.2K
#define	LORA_OPTION			0xC0

LORA_CONFIG g_LoraConfig;
LORA_CONFIG *p_Lora_Config;

static unsigned char s_RecDataBuff[MAX_DATA_BUF]; 

static u8 s_RecDataPackFlag = FALSE;

static u8 s_RegistID = 0;

static u8 s_Data_Interval = 5;

static u8 s_LoraState = TRUE;

static u8 s_LEDFlag = FALSE;

static u8 s_LoraConnectedFlag = FALSE;	//Lora连接成功标志

static u8 s_TCProtocolForLoraRunFlag = FALSE;

void SetTCProtocolForLoraRunFlag(unsigned char isTrue)
{
	s_TCProtocolForLoraRunFlag = isTrue;
	Clear_Uart2Buff();
}

unsigned char GetLEDFlag(void)
{
	return s_LEDFlag;
}

void SetLEDFlag(unsigned char isTrue)
{
	s_LEDFlag = isTrue;
}

unsigned char GetLoraState(void)
{
	return s_LoraState;
}

void LoraObser(unsigned short nMain10ms);

const LORA_CONFIG DefaultLoraConfig = 
{
	0x0f,
	0xff,
	LORA_SPEED_INIT,
	0x13,
	LORA_OPTION,
};

void Lora_Send_Data(unsigned short int Tagetaddr, unsigned char channel, unsigned char *data, unsigned int len)
{
	u8 LoraPack[256];
	u8 Lora_lenth;
	
	LoraPack[0] = Tagetaddr>>8;
	LoraPack[1] = Tagetaddr;
	LoraPack[2] = channel;
	Lora_lenth = 3;
	memcpy(&LoraPack[Lora_lenth], data, len);
	
	Lora_lenth += len;
	
	USART2_DMA_Send(LoraPack, Lora_lenth);
}

void LoraPort_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = LORA_AUX_PIN;	//LORA  AUX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_AUX_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
	

}

static u8 LoraInit()
{
	static u8 s_State = 0, Reset_LoraConfig_Flag = FALSE;
	static unsigned short s_LastTime = 0;
	u8 i, sendbuf[6];
	SYSTEMCONFIG *p_sys;
	const u8 CMD_C1[3] = {0xc1, 0xc1, 0xc1};
	const u8 CMD_C3[3] = {0xc3, 0xc3, 0xc3};
	const u8 CMD_C5[3] = {0xc5, 0xc5, 0xc5};
	const u8 CMD_F3[3] = {0xf3, 0xf3, 0xf3};
	
	p_sys = GetSystemConfig();	
	
	switch(s_State)
	{
		case 0:			
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3) 
			{
				SetTCProtocolForLoraRunFlag(FALSE);
				u1_printf("\r\n [Lora]Sleep\r\n");
				LORA_SLEEP_MODE();
				s_LastTime = GetSystem10msCount();
				s_State++;	
			}	
		break;
		case 1:
			u1_printf("\r\n [Lora]Versions");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_F3, 3);

			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get versions over time\r\n");
					s_LoraState = FALSE;
					break;
				}
			
			}	
			s_State++;
			g_USART2_RX_CNT = 0;		
		break;
		case 2:
			u1_printf("\r\n [Lora]Hardware:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C3, 3);

			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get hardware over time\r\n");
					s_LoraState = FALSE;
					break;
				}

			}	
			s_State++;
			g_USART2_RX_CNT = 0;

		break;
		case 3:
			u1_printf("\r\n [Lora]Batvol:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C5, 3);

			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					u1_printf(" %1.3fV",(g_USART2_RX_BUF[1]*256 + g_USART2_RX_BUF[2])/1000.0);
					g_USART2_RX_CNT = 0;
					u1_printf("\r\n");		
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Batvol over time\r\n");
					s_LoraState = FALSE;
					break;
				}
			}	
			s_State++;
			g_USART2_RX_CNT = 0;
			delay_ms(80);
		break;

		case 4:	
			u1_printf("\r\n [Lora]Config:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C1, 3);

			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;					
					for(i=1; i<g_USART2_RX_CNT; i++)
						u1_printf("%02X ", g_USART2_RX_BUF[i]);
					u1_printf("\r\n");
					g_USART2_RX_CNT = 0;
					if(((p_sys->Node)&0xffff) != ((g_USART2_RX_BUF[1]<<8) + g_USART2_RX_BUF[2]) || p_sys->Channel != g_USART2_RX_BUF[4] || g_USART2_RX_BUF[3] != LORA_SPEED_INIT || g_USART2_RX_BUF[5] != LORA_OPTION )					
					{
						Reset_LoraConfig_Flag = TRUE;
						u1_printf("\r\n Updata Config\r\n");
					}					
					else
					{
						s_State++;
						s_LoraState = TRUE;
						break;
					}
				}
						
					if(Reset_LoraConfig_Flag)		//重新配置参数，重启系统
					{
						Reset_LoraConfig_Flag = FALSE;
						sendbuf[0] = 0xC0;
						sendbuf[1] = (p_sys->Node >> 8)&0xff;
						sendbuf[2] = p_sys->Node &0xff;
						sendbuf[3] = LORA_SPEED_INIT;
						if(p_sys->Channel <= 31)
							sendbuf[4] = p_sys->Channel;
						else
							sendbuf[4] = 15;
						sendbuf[5] = LORA_OPTION;		
						Uart_Send_Data(LORA_COM, sendbuf, sizeof(sendbuf));
						s_LastTime = GetSystem10msCount();	

						while(1)
						{
							if(g_Uart2RxFlag == TRUE)
							{
								g_Uart2RxFlag = FALSE;
								if(g_USART2_RX_BUF[0] == 'O' && g_USART2_RX_BUF[1] == 'K' )
								{				
									s_State++;
									u1_printf("\r\n [Lora]Upadata Config OK\r\n");		

									while (DMA_GetCurrDataCounter(DMA1_Channel4));
									Sys_Soft_Reset();
								}	
								else
								{
									u1_printf("\r\n [Lora]Updata Config fail\r\n");	
									s_State++;
								}
								g_USART2_RX_CNT = 0;								
								break;
							}
							
							if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
							{
								s_LastTime = GetSystem10msCount();					
								u1_printf("\r\n [Lora]Updata Config over time,Fail\r\n");
								return TRUE;
							}
						}
						break;
					}

				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Config over time\r\n");
					s_LoraState = FALSE;
					g_USART2_RX_CNT = 0;
					s_State++;
					break;
				}
		
			}				
		break;
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
			{
				s_LastTime = GetSystem10msCount();		
				u1_printf("\r\n [Lora]Enter Work mode...\r\n");
				LORA_SLEEP_MODE();
				return TRUE;	
			}
		
		default:
			
		break;
	}
	return FALSE;
}


unsigned char JudgmentTimePoint(unsigned char Num, unsigned int *RemainingTime)
{
	unsigned char Minute, Second;
	unsigned short TimePoint = 0;
	static unsigned int s_RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	Minute = RTC_TimeStructure.RTC_Minutes%s_Data_Interval;
	Second = RTC_TimeStructure.RTC_Seconds;
	Num = Num%(s_Data_Interval*60/STOP_TIME);
	TimePoint = Num*STOP_TIME;	

	if(Minute*60 + Second >= TimePoint)
	{
		*RemainingTime = s_Data_Interval*60 - (Minute*60 + Second - TimePoint);		
	}
	else
	{
		*RemainingTime	 = TimePoint - (Minute*60 + Second);
	}
		
	if( (Minute*60 + Second >= TimePoint) && (Minute*60 + Second < TimePoint + STOP_TIME) && (DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) > 30) )
	{
		s_RTCTime = GetRTCSecond();			//到达唤醒时间点
		return TRUE;
	}
	else
	{	
		return FALSE;
	}
}

void LoraProcess(unsigned short nMain10ms)
{
	static u8 RunOnce = FALSE,  s_State = 7, s_RetryCount = 0, s_TimeToRun = FALSE, s_DelayTime = 0, s_SetDelayFlag = FALSE;
	static u16 s_LastTime = 0, s_RebootCount = 0;
	unsigned char SleepTime = 0, InputChannelCount = 0;
	static unsigned int RemainingTime = 0, s_RTCTime = 99999;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;

	
	if(RunOnce == FALSE)
	{	
		RunOnce = TRUE;
		
		LoraPort_Init();		//Lora端口配置
		
		LORA_SLEEP_MODE();
				
		LORA_PWR_OFF();
		
		delay_ms(50);	
		
		LORA_PWR_ON();
	
		delay_ms(50);
		
		USART2_Config(9600);		
				
		u1_printf("\r\n Init LORA\r\n");
		while(LoraInit() == FALSE)		//初始化LORA
		{
			
		}
		
		SetTCProtocolForLoraRunFlag(TRUE);
		
		USART2_Config(LORA_BAND_RATE);
		
		s_LastTime = GetSystem10msCount();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
		srand(RTC_TimeStructure.RTC_Seconds+GetSystem10msCount()); 	
		s_RegistID = rand() % 10;		//初始化时产生0~9的随机序号
		u1_printf("\r\n RandomID:%d\r\n", s_RegistID);
		
		return;
	}
	
	p_sys = GetSystemConfig();
	
	switch(s_State)
	{
		case 1:		
			p_sys = GetSystemConfig();
			if(p_sys->LowpowerFlag == 0)
			{
				s_State++;
			}
			else
			{
				s_State = 8;
			}
		break;
		
		case 2:						//等待传感器检测完成
			if(g_PowerDetectFlag && g_485SensorGetFlag)
			{
				if(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == SET)	//LORA 等待模式切换完成
				{
					s_State++;
				}
				else
				{
					s_State++;
					delay_ms(3);
				}
				
				if(s_SetDelayFlag == FALSE)//只计算一次提前量
				{
					s_SetDelayFlag = TRUE;
					
					InputChannelCount = GetInputChannelCount();
					
					if(InputChannelCount <= 6)	//通道数越大，提前唤醒时间越长
					{
						s_DelayTime = 1;
					}
					else if(InputChannelCount <= 12)
					{
						s_DelayTime = 2;
					}
					else
					{
						s_DelayTime = 3;
					}
					u1_printf("\r\n DelayTime:%d\r\n", s_DelayTime);
				}
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 3:			//上报数据
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 6)
			{
				Mod_Report_Data(LORA_COM);
				//Lora_Report_LowPower_Data();
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();	
			}				
		break;
			
		case 4:
			if(s_RecDataPackFlag)	//收到应答，进入休眠
			{
				LED_NET_FLASH();
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_RebootCount = 0;
				s_RetryCount = 0;
				SetLogErrCode(LOG_CODE_SUCCESS);
				WriteCommunicationSuccessCount();
				StoreOperationalData();
				WriteCommunicationCount();
			}
			else if((s_RetryCount == 0) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= (80 + s_RegistID*2)))//第一次上报等待150ms + 0~500ms
			{
				s_RetryCount++;
				s_State--;		
			}
			else if((s_RetryCount == 1) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 80))//重发等待150ms
			{				
				SetLogErrCode(LOG_CODE_GATEWAYNOACK);
				StoreOperationalData();
				s_RetryCount = 0;
				s_State++;
				LORA_SLEEP_MODE();
				s_RebootCount++;
				u1_printf("\r\n Reported data Fail,Count:%d\r\n", s_RebootCount);			
				WriteCommunicationCount();				
			}
		break;
			
		case 5:
		
			if(s_RebootCount >= 3)	//超时重启
			{
				s_RebootCount = 0;
				u1_printf(" Reported data Fail,Reboot\r\n");
				delay_ms(100);
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();				
			}
			s_LastTime = GetSystem10msCount();
			if(g_PowerDetectFlag && g_485SensorGetFlag)	//传感器检测完成
			{			
				JudgmentTimePoint(s_RegistID, &RemainingTime);	//计算进入停止模式的时间，确保下次唤醒时间在0s开始
				u1_printf("%ds\r\n", RemainingTime);
				if(RemainingTime - g_ReportDataWaitingTime <= 10)		//下次上报数据间隔时间小于10s
				{
					SleepTime = RemainingTime - g_ReportDataWaitingTime - s_DelayTime;
					
					if(SleepTime > 20)	//进入休眠
					{
						s_RTCTime = GetRTCSecond();
						s_TimeToRun = TRUE;
						s_State++;	
						break;
					}
					else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) > 30)		//保证两次上报数据间隔小于30s
					{
						s_RTCTime = GetRTCSecond();
						s_TimeToRun = TRUE;
					}	
					else if(SleepTime == 0)
					{
						SleepTime = STOP_TIME;
					}
				}
				else
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					if(s_LoraConnectedFlag)
					{
						SleepTime = STOP_TIME - RTC_TimeStructure.RTC_Seconds%10;
					}
					else
					{
						SleepTime = ERR_STOP_TIME;
					}
				}
				
					SetStopModeTime(TRUE, SleepTime);	//停止模式入口				
			}
			
			s_State++;		
		break;	
		
		case 6:			
			if(s_TimeToRun)	//判断是否到达上报数据时间点
			{			
				LoraPort_Init();				
				LORA_PWR_ON();
				LORA_SLEEP_MODE();
				delay_ms(20);
				USART2_Config(115200);		
				SetLogErrCode(LOG_CODE_CLEAR);	
				Clear_Flag();
				s_State++;
				s_RetryCount = 0;
				s_LastTime = GetSystem10msCount();
				s_TimeToRun = FALSE;
			}	
			else
			{

				s_LastTime = GetSystem10msCount();
				s_State--;
			}
		break;
					
		case 7:

			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= g_ReportDataWaitingTime*100 + 20)	//等待485传感器读取数据完毕后切换至工作模式，节约电流
			{	
				s_State = 1;	
				LORA_WORK_MODE();					
			}

		break;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////非低功耗模式	

		case 8:
			s_LastTime = GetSystem10msCount();	
			s_State++;
		break;
		
		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= p_sys->Data_interval)
			{
				s_LoraConnectedFlag = FALSE;
				Mod_Report_Data(LORA_COM);
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();	
			}
		break;
		
		case 10:
			if(s_RecDataPackFlag)	//收到应答，进入休眠
			{
				LED_NET_FLASH();
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_RebootCount = 0;
				s_RetryCount = 0;
				SetLogErrCode(LOG_CODE_SUCCESS);
				StoreOperationalData();
			}
			else if((s_RetryCount == 0) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= (60 + s_RegistID*2)))//第一次上报等待150ms + 0~500ms
			{
				s_RetryCount++;
				s_State = 10;		
			}
			else if((s_RetryCount == 1) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 60))//重发等待150ms
			{				
				SetLogErrCode(LOG_CODE_GATEWAYNOACK);
				StoreOperationalData();
				s_RetryCount = 0;
				s_State++;
				s_RebootCount++;
				u1_printf("\r\n Reported data Fail,Count:%d\r\n", s_RebootCount);				
			}
		break;
		
		case 11:
			if(s_RebootCount >= 6)	//超时重启
			{
				s_RebootCount = 0;
				u1_printf(" Reported data Fail,Reboot\r\n");
				delay_ms(100);
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();				
			}
			s_LastTime = GetSystem10msCount();
			
			JudgmentTimePoint(s_RegistID, &RemainingTime);	//计算进入停止模式的时间，确保下次唤醒时间在0s开始

			while(RemainingTime  <= 5)		//下次上报数据间隔时间小于10s
			{
				LORA_WORK_MODE();

				JudgmentTimePoint(s_RegistID, &RemainingTime);
				
				if(RemainingTime <= 1)
				{
					s_State = 12;	
					s_RetryCount = 0;
					break;
				}
			}							
		break;
		
		case 12:
			Mod_Report_Data(LORA_COM);	
			s_RecDataPackFlag = FALSE;
			s_State = 10;
			s_LastTime = GetSystem10msCount();	
		break;		
	}
}

void OnRecLoraData(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg)	
{
	u8 sendbuf[100], sendlenth, ReturnVal = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	static u8 s_GetConfigFlag = FALSE;	//获取配置参数标记
	CLOUD_HDR *hdr;
	static u8 s_buff[sizeof(CLOUD_HDR)];
	SYSTEMCONFIG *p_sys;
	p_sys = GetSystemConfig();
	
	LED_NET_FLASH();		//收到服务器应答，点亮NET
//	p_sys = GetSystemConfig();		
	Clear_Uart2Buff();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
//	u1_printf("\r\n [%02d:%02d:%02d][%d] <-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, p_sys->Device_ID, pMsg->protocol, (int)pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->OPType);
//	for(i=0; i<lenth; i++)
//		u1_printf("%02X ", data[i]);
//	u1_printf("\r\n");
	
	if(pMsg->OPType == CMD_REPORT_D)		//收到数据应答包
	{
		s_RecDataPackFlag = TRUE;
				
		RTC_TimeStructure.RTC_Hours   = pMsg->UserBuf[0];
		RTC_TimeStructure.RTC_Minutes = pMsg->UserBuf[1];
		RTC_TimeStructure.RTC_Seconds = pMsg->UserBuf[2];
		s_RegistID = pMsg->UserBuf[3];			//上报时间节点
		s_Data_Interval = pMsg->UserBuf[4];		//上报间隔

		if(s_Data_Interval < 1)
		{
//			u1_printf("s_Data_Interval = %d, Min data interval:1min...\r\n", s_Data_Interval);
			s_Data_Interval = 1;			
		}
		else if(s_Data_Interval > 120)
		{
//			u1_printf("s_Data_Interval = %d, Max data interval:120min...\r\n", s_Data_Interval);
			s_Data_Interval = 120;
		}

		if(p_sys->LowpowerFlag == 0)
		{
			s_LEDFlag = TRUE;
			
			s_LoraConnectedFlag = TRUE;
		}
		else
		{
			s_Data_Interval = p_sys->Data_interval/60;
		}
		
		if((pMsg->UserBuf[0] == 255) || (pMsg->UserBuf[1] == 255) || (pMsg->UserBuf[2] == 255))	//时分秒为0XFF表明网关不在线
		{
			u1_printf(" Rec gateway ack,but offline...\r\n");
			SetLogErrCode(LOG_CODE_GATEWAYNOLINK);
			StoreOperationalData();
		}
		else 
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
			{	
				if(s_GetConfigFlag == TRUE)		//若收到网关查询配置信息指令，先回复配置信息，再上报数据
				{		
					pMsg = (TC_COMM_MSG *)s_buff;
					System_get_config(LORA_COM, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
					delay_ms(10);
					s_GetConfigFlag = FALSE;
				}
				LORA_SLEEP_MODE();
				RTC_TimeShow();
			}
			else
			{
				u1_printf("Lock-in time fail\r\n");
			}
			
		}
		u1_printf("\r\n 收到网关应答,节点序列号:(%d),上报间隔:%dmin  ", s_RegistID, s_Data_Interval);
		return;
	}
	
	if(pMsg->OPType == CMD_GET_CONFIG)//获取设备配置
	{
		u1_printf(" Get device config..\r\n");
		s_GetConfigFlag = TRUE;		//在回复应答包之前发送配置信息
		
		memcpy(s_buff, pMsg, sizeof(CLOUD_HDR));	//暂存配置信息
//		System_get_config(LORA_COM, pMsg, data, pMsg->Length);
		return;
	}
	
	if(pMsg->OPType == CMD_SET_CONFIG) //设置设备配置
	{
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
		
		p_sys = GetSystemConfig();			
		if(p_sys->LowpowerFlag == 0)
		{
			hdr->device_id = swap_dword((hdr->device_id) | 0x10000000);//低功耗设备
		}
		else if(p_sys->LowpowerFlag == 1)
		{
			hdr->device_id = swap_dword((hdr->device_id));	//非低功耗设备
		}
		
		
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], pMsg->UserBuf, pMsg->Length);
		ReturnVal = System_set_config(LORA_COM, sendbuf, pMsg->Length + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf(" Updata config,wait reboot...\r\n");
		delay_ms(500);
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
}

static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(unsigned char *Data, unsigned short Length, TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[MAX_DATA_BUF];
	uint16   PackLengthgth = 0, i;
	
	memset(PackBuff, sizeof(PackBuff), 0);
	
	UnPackMsg(Data+1, Length-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM2(%d):", PackLengthgth);
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
//协议栈0 应用报文接收观察者处理任务
static void TaskForTCLoraRxObser(void)
{
	static TC_COMM_MSG s_RxFrame;
	static unsigned char PackBuff[MAX_DATA_BUF];
	u16 i, PackStartCount = 0, PackEndCount = 0, RecLength = 0;
	static u8 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10], s_RecStartFlag = FALSE;
	static u16 s_LastTime = 0, RecPackNum = 0;
	static u8 s_BuffStartNum = 0, s_BuffEndNum = 0, s_FullPackFlag = FALSE;
	u8 DataPackCount = 0;
	
	if(g_Uart2RxFlag == TRUE)
	{	
		s_BuffStartNum = PACK_INIT_NUM;
		s_BuffEndNum = 0;
		s_FullPackFlag = FALSE;
		
		if(g_USART2_RX_CNT < MAX_DATA_BUF)
		{
//			memcpy(&PackBuff[RecPackNum], g_USART2_RX_BUF, g_USART2_RX_CNT);    
//			RecPackNum += g_USART2_RX_CNT;
			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				if(g_USART2_RX_BUF[i] == BOF_VAL)
				{
					s_BuffStartNum = i;					
				}
				else if(g_USART2_RX_BUF[i] == EOF_VAL)
				{
					s_BuffEndNum = i;
				}
				
				if((s_BuffStartNum != PACK_INIT_NUM) && (s_BuffEndNum - s_BuffStartNum >= 14) && (s_BuffEndNum > 0))
				{
					DataPackCount = (s_BuffEndNum - s_BuffStartNum + 1);
					memcpy(&PackBuff[s_BuffStartNum], &g_USART2_RX_BUF[s_BuffStartNum], DataPackCount);
					RecPackNum += DataPackCount;
					s_FullPackFlag = TRUE;
					break;
				}				
			}
			
			if(s_FullPackFlag == FALSE)
			{
				memcpy(&PackBuff[RecPackNum], g_USART2_RX_BUF, g_USART2_RX_CNT);	
					
				RecPackNum += g_USART2_RX_CNT;				

			}
			
			if(RecPackNum > MAX_DATA_BUF)
			{
				u1_printf(" Buf Over\r\n");
				s_RecStartFlag = FALSE; 
				RecPackNum = 0;
				memset(PackBuff, 0, sizeof(PackBuff));
				Clear_Uart2Buff();
				return;
			}
			
			s_RecStartFlag = TRUE;	
			s_LastTime = GetSystem10msCount();
		}
		else
		{
			u1_printf(" Data Count Over\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			Clear_Uart2Buff();
			return;
		}
		Clear_Uart2Buff();
	}
	else
	{
//		if(CalculateTime(GetSystem10msCount(), s_nMain10ms) > 150)
//		{
//			s_nMain10ms = GetSystem10msCount();
//			u1_printf(" Nothing...\r\n");
//		}
	}
	
	if(PackBuff[0] == BOF_VAL && PackBuff[RecPackNum - 1] == EOF_VAL && RecPackNum >= 15)
	{
		//连包处理
		PackStartCount = 0;
		PackEndCount = 0;
		
		for(i=0; i<RecPackNum; i++)
		{
			if(PackBuff[i] == 0x7E)	//寻找包头个数
			{
				s_PackStartAddr[PackStartCount] = i;
				PackStartCount++;
			}
			else if(PackBuff[i] == 0x21)	//寻找包尾个数
			{
				s_PackEndAddr[PackEndCount] = i;
				PackEndCount++;
			}
		}
		
		if(PackStartCount == PackEndCount)	//是完整的数据包
		{
			if(PackStartCount > 1)
			{
				u1_printf("\r\n %d Packs\r\n", PackStartCount);
			}
			s_PackCount = PackStartCount;
		}
		else
		{
			u1_printf(" StartCount != EndCount\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			return;
		}
		
		if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
		{
			for(i=0; i<s_PackCount; i++)
			{
				memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));
				
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);
				
				RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
				
				if(RecLength > LORA_BUFF_MAX)
				{
					u1_printf(" Err Lora Length(%d)\r\n", RecLength);
					s_RecStartFlag = FALSE;
					memset(PackBuff, 0, sizeof(PackBuff));
					RecPackNum = 0;
					return;
				}
				
				memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
				
				if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
				{
					OnRecLoraData(USART2, &s_RxFrame);
				}
			}		
		}
		s_RecStartFlag = FALSE;
		memset(PackBuff, 0, sizeof(PackBuff));
		RecPackNum = 0;
	}
	else
	{
			

	}
	
	if(s_RecStartFlag && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 100)) 
	{
		u1_printf(" Lora Rec OverTime(%d)\r\n", RecPackNum);
		for(i=0; i<RecPackNum; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");	
		s_RecStartFlag = FALSE; 
		RecPackNum = 0;
		s_LastTime = GetSystem10msCount();
		memset(PackBuff, 0, sizeof(PackBuff));
		Clear_Uart2Buff();	
	}
}

void TCProtocolForLoraProcess(void)
{
	if(s_TCProtocolForLoraRunFlag)
	{
		TaskForTCLoraRxObser();
	}
}

